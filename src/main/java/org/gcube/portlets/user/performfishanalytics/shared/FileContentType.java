/**
 *
 */
package org.gcube.portlets.user.performfishanalytics.shared;


/**
 * The Enum FileContentType.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * Jan 24, 2019
 */
public enum FileContentType {

	CSV,
	IMAGE,
	UNKNOWN
}
