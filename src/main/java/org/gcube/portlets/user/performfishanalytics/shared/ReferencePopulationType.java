/**
 *
 */
package org.gcube.portlets.user.performfishanalytics.shared;


/**
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jan 17, 2019
 */
public interface ReferencePopulationType {

	public void setPopulationType(PopulationType population);

	public PopulationType getPopulationType();
}
