package org.gcube.portlets.user.performfishanalytics.client.event;

import com.google.gwt.event.shared.EventHandler;

// TODO: Auto-generated Javadoc
/**
 * The Interface LoadBatchesEventHandler.
 */
public interface LoadBatchesEventHandler extends EventHandler {


	/**
	 * On load batches.
	 *
	 * @param loadBatchesEvent the load batches event
	 */
	void onLoadBatches(LoadBatchesEvent loadBatchesEvent);
}