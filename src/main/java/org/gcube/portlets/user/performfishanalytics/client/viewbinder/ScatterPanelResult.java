package org.gcube.portlets.user.performfishanalytics.client.viewbinder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.user.performfishanalytics.client.DataMinerAlgorithms;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant.POPULATION_LEVEL;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsServiceAsync;
import org.gcube.portlets.user.performfishanalytics.client.controllers.PerformFishAnalyticsViewController;
import org.gcube.portlets.user.performfishanalytics.client.resources.PerformFishResources;
import org.gcube.portlets.user.performfishanalytics.client.view.LoaderIcon;
import org.gcube.portlets.user.performfishanalytics.shared.FileContentType;
import org.gcube.portlets.user.performfishanalytics.shared.KPI;
import org.gcube.portlets.user.performfishanalytics.shared.OutputFile;
import org.gcube.portlets.user.performfishanalytics.shared.csv.CSVFile;
import org.gcube.portlets.user.performfishanalytics.shared.dataminer.DataMinerResponse;
import org.gcube.portlets.user.performfishanalytics.shared.performfishservice.PerformFishResponse;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Panel;

public class ScatterPanelResult {

	private PerformFishAnalyticsViewController viewController;
	private int requestId = 0;
	private int outputIteration = 1;
	private int outputNumber = 1;
	private Button savePDFButton;

	public ScatterPanelResult(int reqId, PerformFishAnalyticsViewController viewController,
			final DataMinerAlgorithms algorithm, String focusID, List<KPI> inputKPI, final List<KPI> outputKPI) {
		GWT.log("RequestID: " + reqId);
		requestId = reqId;
		this.viewController = viewController;
		TextResource algDescr = PerformFishResources.INSTANCE.batch_SCATTER();

		String algDesrTxt = algDescr != null ? algDescr.getText() : null;

		// TODO
		final String tabTitle = algorithm.getTitle();
		final String tabDescr = algDesrTxt;

		final SubmitRequestPanel submitRequestPanel = new SubmitRequestPanel("", 1);
		Tab tab = viewController.createTab(tabTitle + " #" + (viewController.currentNumberOfTab() + 1), algDesrTxt,
				submitRequestPanel);

		HorizontalPanel res = new HorizontalPanel();

		savePDFButton = new Button();
		savePDFButton.getElement().getStyle().setMargin(10, Unit.PX);
		savePDFButton.setIcon(IconType.PRINT);
		savePDFButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				saveFilePDF(algorithm.getId(), tabTitle, tabTitle, tabDescr, requestId, outputIteration, outputNumber);
				// dmResponse.getListOutput().size()
			}
		});
		savePDFButton.setVisible(false);

		res.add(savePDFButton);
		submitRequestPanel.addWidget(res);

		if (inputKPI.get(0) == null || inputKPI.get(1) == null) {
			submitRequestPanel
					.setTheTitle("Sorry, something seems wrong, the selected KPIs are not valid. Please try again");
			checkTabSpinner(submitRequestPanel, tab);
			return;
			// Window.alert("Something seems wrong, no selected BatchID, try
			// again");
		}

		String title = "KPI: " + inputKPI.get(0).getName() + " vs " + inputKPI.get(1).getName();
		HTML toBigTitle = new HTML(title);
		toBigTitle.getElement().setId("KPI_Scatter_" + requestId + "_" + outputIteration);
		toBigTitle.getElement().addClassName("to-big-title");
		submitRequestPanel.addWidget(toBigTitle);
		HorizontalPanel hp = new HorizontalPanel();
		hp.getElement().addClassName("ext-horizontal-panel");

		callDataMinerServiceForChart(viewController.getPerformFishResponse(), POPULATION_LEVEL.BATCH, inputKPI,
				outputKPI, algorithm, focusID, submitRequestPanel, hp, tab, outputIteration);
		submitRequestPanel.addWidget(hp);

	}

	private void callDataMinerServiceForChart(PerformFishResponse performFishResponse, POPULATION_LEVEL scalePValue,
			final List<KPI> inputKPI, final List<KPI> outputKPI, final DataMinerAlgorithms chartType,
			final String focusID, final SubmitRequestPanel requestPanel, final ComplexPanel panelContainer,
			final Tab tab, final int oIteration) {

		GWT.log("Read perform fish response: " + performFishResponse);

		String batchTableURL = performFishResponse.getMapParameters().get(PerformFishAnalyticsConstant.BATCHES_TABLE);

		if (batchTableURL == null || batchTableURL.isEmpty())
			Window.alert("Something seems wrong. No batches tables matching with parameter "
					+ PerformFishAnalyticsConstant.BATCHES_TABLE + " returned from service");

		StringBuilder dataInputsFormatter = new StringBuilder();
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_SCALEP_PARAM + "=" + scalePValue.name() + ";");
		String theBatchType = viewController.getForm().getBatchType();
		theBatchType = theBatchType.replace("_CLOSED_BATCHES", ""); // REMOVING
																	// SUFFIX
																	// _CLOSED_BATCHES
																	// FOR
																	// DATAMINER
																	// CALL
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_BATCHTYPE_PARAM + "=" + theBatchType + ";");
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_CHARTTYPE_PARAM + "=" + chartType + ";");
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_FARMFILE_PARAM + "=" + batchTableURL + ";");
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_FOCUS_PARAM + "=" + focusID + ";");

		if (inputKPI != null && inputKPI.size() > 0) {
			String kpiCodes = "";
			for (KPI kpi : inputKPI) {
				kpiCodes += kpi.getCode() + "|";
			}
			// remove last |
			kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);

			GWT.log("Input KPICodes: " + kpiCodes);
			// ADDING KPIs code
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_INPUT_KPI_PARAM + "=" + kpiCodes + ";");

		}

		if (outputKPI != null && outputKPI.size() > 0) {
			String kpiCodes = "";
			for (KPI kpi : outputKPI) {
				kpiCodes += kpi.getCode() + "|";
			}
			// remove last |
			kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);

			GWT.log("Output KPICodes: " + kpiCodes);
			// ADDING KPIs code
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_OUTPUT_KPI_PARAM + "=" + kpiCodes + ";");

		}

		String dataInParameters = dataInputsFormatter.toString();
		GWT.log("Calling DM service with client input parameters: " + dataInParameters);

		Map<String, List<String>> mapParameters = new HashMap<String, List<String>>();
		mapParameters.put(PerformFishAnalyticsConstant.DATA_INPUTS, Arrays.asList(dataInParameters));

		final LoaderIcon loaderIcon = new LoaderIcon("Submitting request to " + chartType.getTitle() + " Analysis...");
		loaderIcon.setVisible(true);
		panelContainer.setVisible(true);
		panelContainer.add(loaderIcon);
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			@Override
			public void execute() {
				loaderIcon.setFocus(true);
			}
		});

		PerformFishAnalyticsServiceAsync.Util.getInstance().callingDataMinerPerformFishAnalysis(mapParameters,
				new AsyncCallback<DataMinerResponse>() {

					@Override
					public void onSuccess(DataMinerResponse dmResponse) {
						loaderIcon.setVisible(false);
						checkTabSpinner(requestPanel, tab);
						// field_unary_algorithm.setVisible(true);
						GWT.log("I'm displaying: " + dmResponse);
						displayOutputFilesAsStaticEntities(dmResponse, chartType, inputKPI, outputKPI, focusID,
								panelContainer, false, oIteration);
					}

					@Override
					public void onFailure(Throwable caught) {
						loaderIcon.setVisible(false);
						checkTabSpinner(requestPanel, tab);
						displayOutputFilesAsStaticEntities(null, chartType, inputKPI, outputKPI, focusID,
								panelContainer, true, oIteration);

					}
				});
	}

	private String getOutputId(int oIteration, String title) {
		String outputId = "OutputId_" + requestId + "_" + oIteration + "_" + 1;
		return outputId;

	}

	private void displayOutputFilesAsStaticEntities(final DataMinerResponse dmResponse,
			final DataMinerAlgorithms chartType, List<KPI> inputKPIs, List<KPI> outputKPIs, final String focusID,
			final Panel container, boolean displayError, final int oIteration) {

		String title = displayError ? "No results " : "";

		if (displayError) {
			Alert alert = new Alert(title);
			alert.setType(AlertType.ERROR);
			alert.setClose(false);
			alert.getElement().getStyle().setMargin(10, Unit.PX);
			container.add(alert);
			return;
		}
		
		savePDFButton.setVisible(true);
		final String toTitle = title;

		for (OutputFile outputFile : dmResponse.getListOutput()) {

			final FileContentType fileContentType = outputFile.getDataType();
			switch (outputFile.getDataType()) {
			case IMAGE:
				PerformFishAnalyticsServiceAsync.Util.getInstance().getImageFile(outputFile,
						new AsyncCallback<String>() {

							@Override
							public void onFailure(Throwable caught) {
								// showAlert(caught.getMessage(),
								// AlertType.ERROR, true,
								// uib_vp_deanalanlysis_request_container);
								Window.alert(caught.getMessage());

							}

							@Override
							public void onSuccess(String base64Content) {
								String title = toTitle;

								String outputId = getOutputId(oIteration, title);
								ShowResult showResult = new ShowResult(outputId, title, fileContentType);
								showResult.showImage(base64Content);
								container.add(showResult);

							}
						});
				break;
			case CSV:
				PerformFishAnalyticsServiceAsync.Util.getInstance().getCSVFile(outputFile, true,
						new AsyncCallback<CSVFile>() {

							@Override
							public void onFailure(Throwable caught) {
								// showAlert(caught.getMessage(),
								// AlertType.ERROR, true,
								// uib_vp_deanalanlysis_request_container);
								Window.alert(caught.getMessage());
							}

							@Override
							public void onSuccess(CSVFile result) {
								GWT.log("Displaying: " + result);

								String cssTableStyle = "simpletable";

								String title = toTitle;

								String outputId = getOutputId(oIteration, title);
								ShowResult showResult = new ShowResult(outputId, title, fileContentType);
								showResult.showCSVFile(result, cssTableStyle);
								container.add(showResult);
							}
						});
				break;

			default:
				break;
			}

		}

	}

	/**
	 * Remove the spinner if all DM responses are returned.
	 *
	 * @param requestPanel
	 *            the request panel
	 * @param tab
	 *            the tab
	 */
	private void checkTabSpinner(SubmitRequestPanel requestPanel, Tab tab) {
		requestPanel.incrementCompletedRequests();
		int completed = requestPanel.getCompletedRequests();
		int total = requestPanel.getTotalRequests();

		if (completed >= total) {
			viewController.noSpinner(tab);
		}
	}

	private static native void saveFilePDF(String chartType, String filename, String tabTitle, String tabDescr,
			int requestId, int outputIteration, int outputNumber)/*-{
		var that = this;
		console.log('saveFilePDF()');
		console.log('requestId: ' + requestId);
		console.log('OutputIteration: ' + outputIteration);
		console.log('OutputNumber:' + outputNumber);

		var tTitle = tabTitle;
		var tDescr = tabDescr;

		var pdoc = new jsPDF("p", "mm", "a4");
		pdoc.setProperties({
			title : 'PerformFish ' + tTitle,
			subject : ' Results',
			author : 'PerformFish',
			keywords : 'PerformFish',
			creator : 'D4Science'
		});

		var lMargin = 15; //left margin in mm
		var rMargin = 15; //right margin in mm
		var tMargin = 15; //top margin in mm
		var bMargin = 15; //bottom margin in mm
		var pdfWidthInMM = 210; // width of A4 in mm
		var pdfHeightInMM = 297; // height of A4 in mm
		var pageCenter = pdfWidthInMM / 2;

		pdoc.setFontSize(24);
		var title = "PerformFish " + tTitle;
		var titleHeight = pdoc.getLineHeight(title) / pdoc.internal.scaleFactor
		var xPos = lMargin;
		var yPos = tMargin;
		pdoc.text(title, pageCenter, yPos, 'center');
		yPos += titleHeight;

		pdoc.setFontSize(10);
		var lineHeight = pdoc.getLineHeight(tDescr) / pdoc.internal.scaleFactor
		var splittedAnalysisDescription = pdoc.splitTextToSize(tDescr,
				(pdfWidthInMM - lMargin - rMargin));
		var lines = splittedAnalysisDescription.length // splitted text is a string array
		var analysisDescriptionHeight = lines * lineHeight

		pdoc.text(splittedAnalysisDescription, xPos, yPos, 'left');
		yPos += analysisDescriptionHeight;

		//yPos += 2;
		//pdoc.text("Analysis:", xPos, yPos, 'left');
		yPos += 6;

		for (var i = 1; i <= outputIteration; i++) {

			var kpiId = 'KPI_Scatter_' + requestId + '_' + i;
			console.log('kpiId: ' + kpiId);

			var kpiName = $doc.getElementById(kpiId);

			yPos += 2;
			pdoc.setFontSize(14);
			pdoc.setFontType("bold");

			var kpiText = kpiName.textContent;
			var kpiHeight = pdoc.getLineHeight(kpiText)
					/ pdoc.internal.scaleFactor;
			var splittedKpiName = pdoc.splitTextToSize(kpiText, (pdfWidthInMM
					- lMargin - rMargin));
			var kpiLines = splittedKpiName.length; // splitted text is a string array
			var kpiLinesHeight = kpiLines * kpiHeight;
			pdoc.text(splittedKpiName, xPos, yPos, 'left');
			yPos += kpiLinesHeight;
			yPos += 6;

			pdoc.setFontSize(10);
			pdoc.setFontType("normal");

			for (var j = 1; j <= outputNumber; j++) {

				var outputId = 'OutputId_' + requestId + '_' + i + '_' + j;
				console.log('OutputId: ' + outputId);

				var resultOutputNumber = $doc.getElementById(outputId);

				console.log('OutputId_: ' + resultOutputNumber.innerHTML);
				var resultType = resultOutputNumber.className;

				console.log('resultOutputNumber className: ' + resultType);

				if (typeof resultType !== 'undefined' && resultType !== null
						&& resultType !== '') {

					if (resultType == 'csv') {
						console.log('Result Type csv: ' + i);
						// foo could get resolved and it's defined

						var childrenTable = resultOutputNumber.children;
						var titleCurrentTable = childrenTable[0].rows[0].cells[0];
						console.log('Title current table: '
								+ titleCurrentTable.textContent);
						yPos += 2;
						pdoc.setFontSize(10);
						pdoc.setFontType("bold");
						pdoc.text(titleCurrentTable.textContent, xPos, yPos,
								'left');
						yPos += 6;

						pdoc.setFontSize(10);
						pdoc.setFontType("normal");

						var secondDiv = childrenTable[0].rows[1].cells[0];
						var secondTable = secondDiv.children[0].children[0];

						console.log('CSV SecondTable: ' + secondTable);

						pdoc.autoTable({
							theme : 'grid',
							startY : yPos,
							pageBreak : 'auto',
							bodyStyles : {
								fontSize : 9
							},
							html : secondTable
						});

						yPos = pdoc.previousAutoTable.finalY + 6;
					} else {
						if (resultType == 'image') {
							console.log('Result Type image: ' + i);
							var childrenTable = resultOutputNumber.children;

							var titleCurrentImage = childrenTable[0].rows[0].cells[0];
							console.log('Title current image: '
									+ titleCurrentImage.textContent);
							yPos += 2;
							pdoc.setFontSize(10);
							pdoc.setFontType("bold");
							pdoc.text(titleCurrentImage.textContent, xPos,
									yPos, 'left');
							yPos += 6;

							pdoc.setFontSize(10);
							pdoc.setFontType("normal");

							var secondDiv = childrenTable[0].rows[1].cells[0];
							var imageElement = secondDiv.getElementsByTagName(
									'img').item(0);
							console.log('Image element: ' + imageElement);
							pdoc.addImage(imageElement, lMargin, yPos);
							console.log('Image Height: ' + imageElement.height);
							yPos += (imageElement.height * 0.26458333);

						} else {
							console.log('Result Type unknow: ' + i);
						}

					}
				} else {
					console.log('Result Type is undefined:' + i);
				}

			}

			if (i == outputNumber) {

			} else {
				if (pdfHeightInMM - yPos < pdfHeightInMM / 4) {
					pdoc.addPage();
					yPos = tMargin; // Restart position
				}
			}

		}

		var numberOfPages=pdoc.internal.getNumberOfPages()
		console.log('NumberOfPages: ' + numberOfPages);
		for (var k = 1; k <= numberOfPages; k++) {
			pdoc.setPage(k);
			console.log('CurrentPage: ' + k);
			var footner = 'Page ' + k + "/"+numberOfPages;
			pdoc.text(footner, pageCenter, pdfHeightInMM - 7, 'center');
			
		}

		console.log('Done');

		pdoc.save(filename);

	}-*/;
}
