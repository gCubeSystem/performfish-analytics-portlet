/**
 *
 */
package org.gcube.portlets.user.performfishanalytics.client.annualcontrollers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.user.performfishanalytics.client.DataMinerAlgorithms;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsServiceAsync;
import org.gcube.portlets.user.performfishanalytics.client.event.CallAlgorithmEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.CallAlgorithmEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadFocusEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadFocusEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadPopulationTypeEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadPopulationTypeEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadSynopticTableEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadSynopticTableEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.PerformFishFieldFormChangedEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.PerformFishFieldFormChangedEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.SelectedKPIEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.SelectedKPIEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.SelectedPopulationTypeEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.SelectedPopulationTypeEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.SubmitRequestEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.SubmitRequestEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.resources.PerformFishResources;
import org.gcube.portlets.user.performfishanalytics.client.viewannualbinder.BoxPlotAnnualPanelResult;
import org.gcube.portlets.user.performfishanalytics.client.viewannualbinder.CorrelationAnnualPanelResult;
import org.gcube.portlets.user.performfishanalytics.client.viewannualbinder.DeaAnnualPanelResult;
import org.gcube.portlets.user.performfishanalytics.client.viewannualbinder.ScatterAnnualPanelResult;
import org.gcube.portlets.user.performfishanalytics.client.viewannualbinder.SpeedometerAnnualPanelResult;
import org.gcube.portlets.user.performfishanalytics.client.viewannualbinder.SynopticTableAnnualPanelResult;
import org.gcube.portlets.user.performfishanalytics.client.viewbinder.SubmitRequestPanel;
import org.gcube.portlets.user.performfishanalytics.shared.KPI;
import org.gcube.portlets.user.performfishanalytics.shared.csv.CSVFile;
import org.gcube.portlets.user.performfishanalytics.shared.csv.CSVRow;
import org.gcube.portlets.user.performfishanalytics.shared.dataminer.DataMinerResponse;
import org.gcube.portlets.user.performfishanalytics.shared.performfishservice.PerformFishInitParameter;
import org.gcube.portlets.user.performfishanalytics.shared.performfishservice.PerformFishResponse;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

// TODO: Auto-generated Javadoc
/**
 * The Class PerformFishAnalyticsController.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jan 16, 2019
 */
public class PerformFishAnnualAnalyticsController {

	/** The Constant eventBus. */

	public final static HandlerManager eventBus = new HandlerManager(null);

	/** The view controller. */
	private PerformFishAnnualAnalyticsViewController viewAnnualController;

	/** The decrypt parameters. */
	private PerformFishInitParameter decryptParameters;

	private PerformFishResponse thePerformFishResponse;

	/** The focus. */
	// This value is read from the first value of column "FARM" contained in
	// the table AnnualTable_internal.csv returned by PerformFish Service
	private String theFocusValue = null;

	private static int requestId = 0;

	/**
	 * Instantiates a new perform fish analytics controller.
	 */
	public PerformFishAnnualAnalyticsController() {

		registerHandlers();
	}

	/**
	 * Sets the inits the parmaters.
	 *
	 * @param result
	 *            the new inits the parmaters
	 */
	public void setInitParmaters(PerformFishInitParameter result) {
		this.decryptParameters = result;
		viewAnnualController = new PerformFishAnnualAnalyticsViewController();
	}

	/**
	 * Register handlers.
	 */
	private void registerHandlers() {

		eventBus.addHandler(LoadPopulationTypeEvent.TYPE, new LoadPopulationTypeEventHandler() {

			@Override
			public void onLoadPopulationType(LoadPopulationTypeEvent loadPopulationEvent) {

				viewAnnualController.loadPopulationTypeForLevelAndBatchType(loadPopulationEvent.getPopulationName(),
						decryptParameters);

			}
		});

		eventBus.addHandler(CallAlgorithmEvent.TYPE, new CallAlgorithmEventHandler() {

			@Override
			public void onCall(CallAlgorithmEvent callAlgorithmEvent) {

				callAlgorithm(callAlgorithmEvent.getAlgorithm(), callAlgorithmEvent.getFocusID(),
						callAlgorithmEvent.getInputKPI(), callAlgorithmEvent.getOutputKPI());
			}
		});
		
	

		eventBus.addHandler(SelectedPopulationTypeEvent.TYPE, new SelectedPopulationTypeEventHandler() {

			@Override
			public void onSelectedPopulationType(SelectedPopulationTypeEvent selectedPopulationTypeEvent) {

				viewAnnualController
						.setRootPopulationTypeForKPIs(selectedPopulationTypeEvent.getSelectedPopulationType());
			}
		});

		eventBus.addHandler(SelectedKPIEvent.TYPE, new SelectedKPIEventHandler() {

			@Override
			public void onSelectedKPI(SelectedKPIEvent selectedKPI) {
				GWT.log("Selected KPI: " + selectedKPI);
				viewAnnualController.manageKPI(selectedKPI.getKpi(), selectedKPI.isChecked(),
						selectedKPI.getSelectedPopulationType());
				eventBus.fireEvent(new PerformFishFieldFormChangedEvent(null));

			}
		});

		eventBus.addHandler(PerformFishFieldFormChangedEvent.TYPE, new PerformFishFieldFormChangedEventHandler() {

			@Override
			public void onFieldFormChanged(PerformFishFieldFormChangedEvent performFishFieldFormChangedEvent) {

				boolean isValidForm = viewAnnualController.validatePerformFishInputFields();

				if (isValidForm) {
					// boolean isKPIsSelected =
					// viewAnnualController.validateKPIFields();
					int selectedKPIsSize = viewAnnualController.getSelectedKPIs().size();
					viewAnnualController.manageAlgorithmsSubmit(selectedKPIsSize);
					viewAnnualController.enableSynopticTable(true);
				} else {
					// viewController.enableLoadBatches(false);
					viewAnnualController.enableAllAlgorithmsForSubmit(false);
					viewAnnualController.enableSynopticTable(false);
				}
			}
		});

		eventBus.addHandler(LoadFocusEvent.TYPE, new LoadFocusEventHandler() {

			@Override
			public void onLoadFocusEvent(LoadFocusEvent loadFocusEvent) {
				manageLoadFocus();
			}

		});

		//
		eventBus.addHandler(SubmitRequestEvent.TYPE, new SubmitRequestEventHandler() {

			@Override
			public void onSubmitRequest(SubmitRequestEvent submitRequestEvent) {

				// boolean isValidBatchId =
				// viewAnnualController.validateBatchIdSelection();

				boolean isValidKPI = viewAnnualController.validateKPIFields();

				List<KPI> selectedKPI = viewAnnualController.getSelectedKPIs();
				viewAnnualController.manageAlgorithmsSubmit(selectedKPI.size());

				if (theFocusValue != null) {

					if (isValidKPI) {
						submitRequestToDM(submitRequestEvent.getChartType());
					}
				}

				return;
			}
		});

		eventBus.addHandler(LoadSynopticTableEvent.TYPE, new LoadSynopticTableEventHandler() {

			@Override
			public void onLoadSynopticTable(LoadSynopticTableEvent loadSynopticTableEvent) {

				callAlgorithmSynopticTableFarm();
			}
		});

	}

	private void manageLoadFocus() {
		final Map<String, List<String>> mapParameters = new HashMap<String, List<String>>();
		String farmId = decryptParameters.getParameters().get(PerformFishAnalyticsConstant.PERFORM_FISH_FARMID_PARAM);
		String batchType = decryptParameters.getParameters()
				.get(PerformFishAnalyticsConstant.PERFORM_FISH_BATCH_TYPE_PARAM);

		// String batchType =
		// viewAnnualController.getForm().getBatchType();

		// List<String> listYear =
		// viewAnnualController.getForm().getYear();

		mapParameters.put(PerformFishAnalyticsConstant.PERFORM_FISH_FARMID_PARAM, Arrays.asList(farmId));
		mapParameters.put(PerformFishAnalyticsConstant.PERFORM_FISH_BATCH_TYPE_PARAM, Arrays.asList(batchType));

		PerformFishAnalyticsServiceAsync.Util.getInstance().submitRequestToPerformFishService(mapParameters,
				new AsyncCallback<PerformFishResponse>() {

					@Override
					public void onFailure(Throwable caught) {
						// modal.hide();
						Window.alert(caught.getMessage());
					}

					@Override
					public void onSuccess(PerformFishResponse performFishResponse) {
						thePerformFishResponse = performFishResponse;
						GWT.log("PerformFish Response: " + performFishResponse);

						final String pfTableName = PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.AnnualTable_internal
								.getPerformFishTable();

						String fileURL = performFishResponse.getMapParameters().get(pfTableName);

						GWT.log(pfTableName + " is: " + fileURL);

						// Managing the Perform Fish Service Response
						if (fileURL == null) {
							viewAnnualController.showAlert("No table found by searching for name: "
									+ PerformFishAnalyticsConstant.BATCHES_TABLE_INTERNAL, AlertType.ERROR);
						} else {

							PerformFishAnalyticsServiceAsync.Util.getInstance().readCSVFile(fileURL,
									new AsyncCallback<CSVFile>() {

										@Override
										public void onFailure(Throwable caught) {
											Window.alert(caught.getMessage());

										}

										@Override
										public void onSuccess(CSVFile result) {

											if (result == null) {
												viewAnnualController.showAlert(
														"The focus was not found in the table " + pfTableName,
														AlertType.ERROR);
												return;
											}

											int indexOfFARM = result.getHeaderRow().getListValues()
													.indexOf(PerformFishAnalyticsConstant.POPULATION_LEVEL.FARM.name());

											GWT.log("The index of column "
													+ PerformFishAnalyticsConstant.POPULATION_LEVEL.FARM.name() + " is "
													+ indexOfFARM);

											if (indexOfFARM > -1) {
												List<CSVRow> rows = result.getValueRows();

												if (rows == null || rows.isEmpty()) {
													viewAnnualController.showAlert(
															"No valid focus was found in the table " + pfTableName,
															AlertType.ERROR);
													return;
												}

												String focusValue = null;
												// IN THE COLUMN WITH
												// HEADER 'FARM' THE
												// FOCUS VALUE IS THE
												// SAME FOR ALL ROWS
												for (CSVRow row : rows) {
													focusValue = row.getListValues().get(indexOfFARM);
													if (focusValue != null && !focusValue.isEmpty())
														break;
												}

												if (focusValue == null) {
													viewAnnualController.showAlert(
															"No valid focus was found in the table " + pfTableName,
															AlertType.ERROR);
													return;
												}

												theFocusValue = focusValue;
												GWT.log("Loaded the focus value: " + theFocusValue);
											}
										}
									});

						}
					}
				});
	}

	/**
	 * Call algorithm synoptic table farm.
	 */
	protected void callAlgorithmSynopticTableFarm() {

		requestId++;
		@SuppressWarnings("unused")
		SynopticTableAnnualPanelResult synopticTablePanelResult = new SynopticTableAnnualPanelResult(requestId,
				viewAnnualController, thePerformFishResponse);
	}

	/**
	 * Submit request to DM.
	 *
	 * @param dmAlgorithm
	 *            the dm algorithm
	 */
	private void submitRequestToDM(DataMinerAlgorithms dmAlgorithm) {

		switch (dmAlgorithm) {

		case BOXPLOT:
			// UNARY
			callAlgorithm(dmAlgorithm, theFocusValue, viewAnnualController.getSelectedKPIs(), null);
			break;

		case SCATTER:
			if (viewAnnualController.getSelectedKPIs().size() == 2)
				callAlgorithm(dmAlgorithm, theFocusValue, viewAnnualController.getSelectedKPIs(), null);
			else
				Window.alert("Something seems wrong... You must select exactly two KPIs to execute the " + dmAlgorithm);
			break;
		case CORRELATION:
			// callDataMinerServiceForChartTypeCorrelation(thePerformFishResponse,
			// viewController.getRequestMapParameters());
			callDataMinerServiceForChartTypeCorrelation(theFocusValue, viewAnnualController.getSelectedKPIs(),
					viewAnnualController.getSelectedKPIs());
			// callAlgorithm(dmAlgorithm, theFocusValue,
			// viewAnnualController.getSelectedKPIs(), null);
			break;
		case SPEEDOMETER:
			// UNARY
			callAlgorithm(dmAlgorithm, theFocusValue, viewAnnualController.getSelectedKPIs(), null);
			break;
		case DEA_ANALYSIS:
			createTabToSelectIOParametersForDeaAnnualAnalysis(theFocusValue, viewAnnualController.getSelectedKPIs());
			break;
		case DEA_CHART:
			break;
		case PERFORMFISH_SYNOPTICTABLE_BATCH:
			break;
		case PERFORMFISH_SYNOPTICTABLE_BATCH_HATCHERY:
			break;
		case PERFORMFISH_SYNOPTICTABLE_BATCH_PREGROW:
			break;
		case PERFORMFISH_SYNOPTIC_TABLE_FARM:
			break;
		default:
			break;

		}

	}

	/**
	 * Call data miner service for chart type correlation.
	 *
	 * @param focusID
	 *            the focus ID
	 * @param inputKPI
	 *            the input KPI
	 * @param outputKPI
	 *            the output KPI
	 */
	private void createTabToSelectIOParametersForDeaAnnualAnalysis(String focusID, final List<KPI> listKPI) {

		DataMinerInputParameters dmInputParameters = new DataMinerInputParameters(thePerformFishResponse,
				viewAnnualController.getSelectedYears(), listKPI, listKPI, DataMinerAlgorithms.DEA_ANALYSIS, focusID);

		GWT.log("Building DM request with input parameters: " + dmInputParameters);

		StringBuilder dataInputsFormatter = new StringBuilder();
		// dataInputsFormatter.append(
		// PerformFishAnalyticsConstant.DM_SCALEP_PARAM + "=" +
		// viewAnnualController.getForm().getLevel() + ";");
		dataInputsFormatter
				.append(PerformFishAnalyticsConstant.DM_CHARTTYPE_PARAM + "=" + dmInputParameters.getChartType() + ";");

		String yearsValue = "";
		for (String year : dmInputParameters.getSelectedYears()) {
			yearsValue += year + "|";
		}
		yearsValue = yearsValue.substring(0, yearsValue.length() - 1);

		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_YEARS_PARAM + "=" + yearsValue + ";");
		dataInputsFormatter
				.append(PerformFishAnalyticsConstant.DM_FOCUS_PARAM + "=" + dmInputParameters.getFocusID() + ";");

		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_APDISEASES + "=null;");
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_ABDISEASES + "=null;");

		if (dmInputParameters.getInputKPI() != null && dmInputParameters.getInputKPI().size() > 0) {
			String kpiCodes = "";
			for (KPI kpi : dmInputParameters.getInputKPI()) {
				kpiCodes += kpi.getCode() + "|";
			}
			// remove last |
			kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);

			GWT.log("Input KPICodes: " + kpiCodes);
			// ADDING KPIs code
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_INPUT_KPI_PARAM + "=" + kpiCodes + ";");

		}

		if (dmInputParameters.getOutputKPI() != null && dmInputParameters.getOutputKPI().size() > 0) {
			String kpiCodes = "";
			for (KPI kpi : dmInputParameters.getOutputKPI()) {
				kpiCodes += kpi.getCode() + "|";
			}
			// remove last |
			kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);

			GWT.log("Output KPICodes: " + kpiCodes);
			// ADDING KPIs code
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_OUTPUT_KPI_PARAM + "=" + kpiCodes + ";");
		}

		Map<String, String> performFishResponseMap = dmInputParameters.getPerformFishResponse().getMapParameters();

		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.LethalIncidentsTable);
		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.AnnualTable);
		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.AntibioticsTable);
		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.AntiparasiticTable);

		String dataInParameters = dataInputsFormatter.toString();
		GWT.log("Calling DM service for annual with client input parameters: " + dataInParameters);

		final Map<String, List<String>> mapParameters = new HashMap<String, List<String>>();
		mapParameters.put(PerformFishAnalyticsConstant.DATA_INPUTS, Arrays.asList(dataInParameters));

		final SubmitRequestPanel submitRequestPanel = new SubmitRequestPanel("", 1);
		submitRequestPanel.showLoader(false, null);

		String tabTitle = DataMinerAlgorithms.DEA_ANALYSIS.getTitle();
		final Tab tab = viewAnnualController.createTab(
				tabTitle + " #" + (viewAnnualController.currentNumberOfTab() + 1),
				PerformFishResources.INSTANCE.farm_DEAANALYSIS().getText(), submitRequestPanel);
		viewAnnualController.noSpinner(tab);

		final List<KPI> selectedKPI = new ArrayList<KPI>(viewAnnualController.getSelectedKPIs());
		final List<String> focusIds = new ArrayList<String>();
		focusIds.add(focusID);

		DeaAnnualPanelResult deaAnnualPanelResult = new DeaAnnualPanelResult();

		deaAnnualPanelResult.addSelectedKPIs(selectedKPI);
		deaAnnualPanelResult.addParameters(PerformFishAnalyticsConstant.DATA_INPUTS, mapParameters,
				viewAnnualController.getForm().getBatchType());
		submitRequestPanel.addWidget(deaAnnualPanelResult);
	}

	/**
	 * Call data miner service for chart type correlation.
	 *
	 * @param focusID
	 *            the focus ID
	 * @param inputKPI
	 *            the input KPI
	 * @param outputKPI
	 *            the output KPI
	 */
	private void callDataMinerServiceForChartTypeCorrelation(String focusID, final List<KPI> inputKPI,
			final List<KPI> outputKPI) {

		DataMinerInputParameters dmInputParameters = new DataMinerInputParameters(thePerformFishResponse,
				viewAnnualController.getSelectedYears(), inputKPI, outputKPI, DataMinerAlgorithms.CORRELATION, focusID);

		GWT.log("Building DM request with input parameters: " + dmInputParameters);

		StringBuilder dataInputsFormatter = new StringBuilder();

		dataInputsFormatter
				.append(PerformFishAnalyticsConstant.DM_CHARTTYPE_PARAM + "=" + dmInputParameters.getChartType() + ";");

		String yearsValue = "";
		for (String year : dmInputParameters.getSelectedYears()) {
			yearsValue += year + "|";
		}
		yearsValue = yearsValue.substring(0, yearsValue.length() - 1);

		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_YEARS_PARAM + "=" + yearsValue + ";");
		dataInputsFormatter
				.append(PerformFishAnalyticsConstant.DM_FOCUS_PARAM + "=" + dmInputParameters.getFocusID() + ";");
		// dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_FOCUS_PARAM+"=ID;");

		if (dmInputParameters.getInputKPI() != null && dmInputParameters.getInputKPI().size() > 0) {
			String kpiCodes = "";
			for (KPI kpi : dmInputParameters.getInputKPI()) {
				kpiCodes += kpi.getCode() + "|";
			}
			// remove last |
			kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);

			GWT.log("Input KPICodes: " + kpiCodes);
			// ADDING KPIs code
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_INPUT_KPI_PARAM + "=" + kpiCodes + ";");

		}

		if (dmInputParameters.getOutputKPI() != null && dmInputParameters.getOutputKPI().size() > 0) {
			String kpiCodes = "";
			for (KPI kpi : dmInputParameters.getOutputKPI()) {
				kpiCodes += kpi.getCode() + "|";
			}
			// remove last |
			kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);

			GWT.log("Output KPICodes: " + kpiCodes);
			// ADDING KPIs code
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_OUTPUT_KPI_PARAM + "=" + kpiCodes + ";");
		}

		Map<String, String> performFishResponseMap = dmInputParameters.getPerformFishResponse().getMapParameters();

		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.LethalIncidentsTable);
		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.AnnualTable);
		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.AntibioticsTable);
		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.AntiparasiticTable);

		String dataInParameters = dataInputsFormatter.toString();
		GWT.log("Calling DM service with client input parameters: " + dataInParameters);

		final Map<String, List<String>> mapParameters = new HashMap<String, List<String>>();
		mapParameters.put(PerformFishAnalyticsConstant.DATA_INPUTS, Arrays.asList(dataInParameters));

		final SubmitRequestPanel submitRequestPanel = new SubmitRequestPanel("", 1);
		submitRequestPanel.showLoader(true,
				"Submitting " + DataMinerAlgorithms.CORRELATION.getTitle() + " request to DM Service...");
		final String tabTitle = DataMinerAlgorithms.CORRELATION.getTitle();
		final String tabDescription = PerformFishResources.INSTANCE.farm_CORRELATION().getText();

		final Tab tab = viewAnnualController.createTab(
				tabTitle + " #" + (viewAnnualController.currentNumberOfTab() + 1), tabDescription, submitRequestPanel);

		requestId++;
		PerformFishAnalyticsServiceAsync.Util.getInstance().callingDataMinerPerformFishAnnualCorrelationAnalysis(
				dmInputParameters.getPerformFishResponse(), mapParameters, new AsyncCallback<DataMinerResponse>() {

					@Override
					public void onSuccess(DataMinerResponse dmResponse) {
						submitRequestPanel.showLoader(false, null);
						checkTabSpinner(submitRequestPanel, tab);
						CorrelationAnnualPanelResult analyticsPanelResult = new CorrelationAnnualPanelResult(requestId,
								tabTitle, tabDescription);
						analyticsPanelResult.addSelectedKPIs(inputKPI);
						analyticsPanelResult.addListBatchIds(Arrays.asList(theFocusValue));
						analyticsPanelResult.addParameters(PerformFishAnalyticsConstant.DATA_INPUTS, mapParameters,
								viewAnnualController.getForm().getBatchType());
						analyticsPanelResult.addResults(dmResponse);
						submitRequestPanel.addWidget(analyticsPanelResult);

					}

					@Override
					public void onFailure(Throwable caught) {
						GWT.log(caught.toString());
						submitRequestPanel.showLoader(false, null);
						checkTabSpinner(submitRequestPanel, tab);
						try {
							CorrelationAnnualPanelResult analyticsPanelResult = new CorrelationAnnualPanelResult(
									requestId, tabTitle, tabDescription);
							analyticsPanelResult.addSelectedKPIs(inputKPI);
							analyticsPanelResult.addListBatchIds(Arrays.asList(theFocusValue));
							analyticsPanelResult.addParameters(PerformFishAnalyticsConstant.DATA_INPUTS, mapParameters,
									viewAnnualController.getForm().getBatchType());
							submitRequestPanel.addWidget(analyticsPanelResult);
						} catch (Exception e) {
							GWT.log(e.getLocalizedMessage(), e);
						}

						Alert error = new Alert(caught.getMessage());
						error.setClose(false);
						error.setType(AlertType.ERROR);
						submitRequestPanel.addWidget(error);
					}
				});
	}

	/**
	 * Creating new TAB and calling DM algorithm.
	 *
	 * @param algorithm
	 *            the algorithm
	 * @param focusID
	 *            the focus id
	 * @param inputKPI
	 *            the input kpi
	 * @param outputKPI
	 *            the output kpi
	 */
	private void callAlgorithm(DataMinerAlgorithms algorithm, String focusID, List<KPI> inputKPI,
			final List<KPI> outputKPI) {

		requestId++;
		switch (algorithm) {
		case BOXPLOT:
			@SuppressWarnings("unused")
			BoxPlotAnnualPanelResult boxPlotAnnualPanelResult = new BoxPlotAnnualPanelResult(requestId,
					viewAnnualController, thePerformFishResponse, algorithm, focusID, inputKPI, outputKPI);
			break;
		case SPEEDOMETER:
			@SuppressWarnings("unused")
			SpeedometerAnnualPanelResult speedometerAnnualPanelResult = new SpeedometerAnnualPanelResult(requestId,
					viewAnnualController, thePerformFishResponse, algorithm, focusID, inputKPI, outputKPI);

			break;
		case SCATTER:
			@SuppressWarnings("unused")
			ScatterAnnualPanelResult scatterAnnualPanelResult = new ScatterAnnualPanelResult(requestId,
					viewAnnualController, thePerformFishResponse, algorithm, focusID, inputKPI, outputKPI);
			break;
		default:
			break;

		}
	}
	
	
	

	/**
	 * Append DM input table.
	 *
	 * @param performFishResponseMap
	 *            the perform fish response map
	 * @param dataInputsFormatter
	 *            the data inputs formatter
	 * @param table
	 *            the table
	 * @return the string builder
	 */
	private StringBuilder appendDMInputTable(Map<String, String> performFishResponseMap,
			StringBuilder dataInputsFormatter, PFSERVICE_TO_DM_MAPPING_TABLE table) {

		String toDMInputTable = performFishResponseMap.get(table.getPerformFishTable());

		if (toDMInputTable != null && !toDMInputTable.isEmpty()) {
			dataInputsFormatter.append(table.getDataMinerTable() + "=" + toDMInputTable + ";");
		}

		return dataInputsFormatter;
	}

	/**
	 * Remove the spinner if all DM responses are returned.
	 *
	 * @param requestPanel
	 *            the request panel
	 * @param tab
	 *            the tab
	 */
	private void checkTabSpinner(SubmitRequestPanel requestPanel, Tab tab) {
		requestPanel.incrementCompletedRequests();
		int completed = requestPanel.getCompletedRequests();
		int total = requestPanel.getTotalRequests();

		if (completed >= total) {
			viewAnnualController.noSpinner(tab);
		}
	}

}
