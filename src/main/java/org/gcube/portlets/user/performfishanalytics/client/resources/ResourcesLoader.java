package org.gcube.portlets.user.performfishanalytics.client.resources;

import com.google.gwt.core.client.ScriptInjector;

/**
 * Resources Loader
 * 
 * @author Giancarlo Panichi
 *
 */
public class ResourcesLoader {

	public ResourcesLoader() {
		ScriptInjector.fromString(PerformFishResources.INSTANCE.es6PromiseAuto().getText()).inject();
		
		ScriptInjector.fromString(PerformFishResources.INSTANCE.jsPDF().getText()).inject();
		
		ScriptInjector.fromString(PerformFishResources.INSTANCE.autotable().getText()).inject();
		
		ScriptInjector.fromString(PerformFishResources.INSTANCE.html2canvas().getText()).inject();
		
		//ScriptInjector.fromString(PerformFishResources.INSTANCE.html2pdf().getText()).inject();
		
	}
}
