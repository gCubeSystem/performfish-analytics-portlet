/**
 *
 */
package org.gcube.portlets.user.performfishanalytics.client.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.user.performfishanalytics.client.DataMinerAlgorithms;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant.POPULATION_LEVEL;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsServiceAsync;
import org.gcube.portlets.user.performfishanalytics.client.event.AddedBatchIdEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.AddedBatchIdEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.CallAlgorithmEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.CallAlgorithmEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadBatchesEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadBatchesEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadPopulationTypeEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadPopulationTypeEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadSynopticTableEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.LoadSynopticTableEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.PerformFishFieldFormChangedEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.PerformFishFieldFormChangedEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.SelectedKPIEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.SelectedKPIEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.SelectedPopulationTypeEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.SelectedPopulationTypeEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.event.SubmitRequestEvent;
import org.gcube.portlets.user.performfishanalytics.client.event.SubmitRequestEventHandler;
import org.gcube.portlets.user.performfishanalytics.client.resources.PerformFishResources;
import org.gcube.portlets.user.performfishanalytics.client.view.LoaderIcon;
import org.gcube.portlets.user.performfishanalytics.client.viewbinder.CorrelationPanelResult;
import org.gcube.portlets.user.performfishanalytics.client.viewbinder.BoxPlotPanelResult;
import org.gcube.portlets.user.performfishanalytics.client.viewbinder.DeaPanelResult;
import org.gcube.portlets.user.performfishanalytics.client.viewbinder.ScatterPanelResult;
import org.gcube.portlets.user.performfishanalytics.client.viewbinder.SpeedometerPanelResult;
import org.gcube.portlets.user.performfishanalytics.client.viewbinder.SubmitRequestPanel;
import org.gcube.portlets.user.performfishanalytics.client.viewbinder.SynopticTablePanelResult;
import org.gcube.portlets.user.performfishanalytics.shared.KPI;
import org.gcube.portlets.user.performfishanalytics.shared.PopulationType;
import org.gcube.portlets.user.performfishanalytics.shared.dataminer.DataMinerResponse;
import org.gcube.portlets.user.performfishanalytics.shared.performfishservice.PerformFishInitParameter;
import org.gcube.portlets.user.performfishanalytics.shared.performfishservice.PerformFishResponse;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * The Class PerformFishAnalyticsController.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jan 16, 2019
 */
public class PerformFishAnalyticsController {

	/** The Constant eventBus. */

	public final static HandlerManager eventBus = new HandlerManager(null);

	/** The view controller. */
	private PerformFishAnalyticsViewController viewController;

	/** The decrypt parameters. */
	private PerformFishInitParameter decryptParameters;

	private static int requestId = 0;

	/**
	 * Instantiates a new perform fish analytics controller.
	 */
	public PerformFishAnalyticsController() {

		registerHandlers();
		viewController = new PerformFishAnalyticsViewController();
	}

	/**
	 * Sets the inits the parmaters.
	 *
	 * @param result
	 *            the new inits the parmaters
	 */
	public void setInitParmaters(PerformFishInitParameter result) {
		this.decryptParameters = result;
	}

	/**
	 * Register handlers.
	 */
	private void registerHandlers() {

		eventBus.addHandler(LoadPopulationTypeEvent.TYPE, new LoadPopulationTypeEventHandler() {

			@Override
			public void onLoadPopulationType(LoadPopulationTypeEvent loadPopulationEvent) {

				viewController.loadPopulationTypeForLevelAndBatchType(loadPopulationEvent.getPopulationName(),
						decryptParameters);

				// if(loadPopulationEvent.getPopulationName().equals(POPULATION_LEVEL.BATCH.name()))
				// {
				//
				// }else {
				// viewAnnualController.loadPopulationTypeForLevelAndBatchType(loadPopulationEvent.getPopulationName(),
				// decryptParameters);
				// }
			}
		});

		eventBus.addHandler(CallAlgorithmEvent.TYPE, new CallAlgorithmEventHandler() {

			@Override
			public void onCall(CallAlgorithmEvent callAlgorithmEvent) {

				callAlgorithm(callAlgorithmEvent.getAlgorithm(), callAlgorithmEvent.getFocusID(),
						callAlgorithmEvent.getInputKPI(), callAlgorithmEvent.getOutputKPI());
			}
		});

		eventBus.addHandler(SelectedPopulationTypeEvent.TYPE, new SelectedPopulationTypeEventHandler() {

			@Override
			public void onSelectedPopulationType(SelectedPopulationTypeEvent selectedPopulationTypeEvent) {

				PopulationType opt = selectedPopulationTypeEvent.getSelectedPopulationType();
				viewController.setRootPopulationTypeForKPIs(selectedPopulationTypeEvent.getSelectedPopulationType());

				if (opt.getName().equalsIgnoreCase(
						PerformFishAnalyticsConstant.BATCH_LEVEL.GROW_OUT_INDIVIDUAL_CLOSED_BATCHES.name())
						|| opt.getName()
								.equalsIgnoreCase(PerformFishAnalyticsConstant.BATCH_LEVEL.GROW_OUT_INDIVIDUAL.name())
						|| opt.getName().equals(
								PerformFishAnalyticsConstant.BATCH_LEVEL.HATCHERY_INDIVIDUAL_CLOSED_BATCHES.name())
						|| opt.getName().equals(PerformFishAnalyticsConstant.BATCH_LEVEL.HATCHERY_INDIVIDUAL.name())
						|| opt.getName().equals(PerformFishAnalyticsConstant.BATCH_LEVEL.PRE_ONGROWING.name())
						|| opt.getName()
								.equals(PerformFishAnalyticsConstant.BATCH_LEVEL.PRE_ONGROWING_CLOSED_BATCHES.name())) {
					viewController.visibleSynopticTable(true);
				} else {
					viewController.visibleSynopticTable(false);
				}
			}
		});

		eventBus.addHandler(SelectedKPIEvent.TYPE, new SelectedKPIEventHandler() {

			@Override
			public void onSelectedKPI(SelectedKPIEvent selectedKPI) {

				viewController.manageKPI(selectedKPI.getKpi(), selectedKPI.isChecked(),
						selectedKPI.getSelectedPopulationType());
				int selectedKPIsSize = viewController.getSelectedKPIs().size();
				// viewController.manageAlgorithmsSubmit(selectedKPIsSize);
				String selectedBatchId = viewController.getSelectedBatchID();
				if (selectedBatchId == null || selectedBatchId.isEmpty()) {
					viewController.enableAllAlgorithmsForSubmit(false);
				} else
					viewController.manageAlgorithmsSubmit(selectedKPIsSize);
			}
		});

		eventBus.addHandler(LoadBatchesEvent.TYPE, new LoadBatchesEventHandler() {

			@Override
			public void onLoadBatches(LoadBatchesEvent loadBatchesEvent) {

				viewController.hideErrors();

				final Map<String, List<String>> mapParameters = new HashMap<String, List<String>>();
				String farmId = decryptParameters.getParameters()
						.get(PerformFishAnalyticsConstant.PERFORM_FISH_FARMID_PARAM);
				String batchType = viewController.getForm().getBatchType();
				String species = viewController.getForm().getSpecies();
				List<String> listArea = viewController.getForm().getArea();
				List<String> listPeriod = viewController.getForm().getPeriod();
				List<String> listQuarter = viewController.getForm().getQuarter();

				mapParameters.put(PerformFishAnalyticsConstant.PERFORM_FISH_FARMID_PARAM, Arrays.asList(farmId));
				mapParameters.put(PerformFishAnalyticsConstant.PERFORM_FISH_BATCH_TYPE_PARAM, Arrays.asList(batchType));
				mapParameters.put(PerformFishAnalyticsConstant.PERFORM_FISH_SPECIES_ID_PARAM, Arrays.asList(species));

				if (!listArea.isEmpty()) {
					mapParameters.put(PerformFishAnalyticsConstant.PERFORM_FISH_AREA_PARAM, listArea);
				}
				if (!listPeriod.isEmpty()) {
					mapParameters.put(PerformFishAnalyticsConstant.PERFORM_FISH_PERIOD_PARAM, listPeriod);
				}
				if (!listQuarter.isEmpty()) {
					mapParameters.put(PerformFishAnalyticsConstant.PERFORM_FISH_QUARTER_PARAM, listQuarter);
				}

				final Modal modal = new Modal(true);
				modal.setCloseVisible(false);
				modal.hide(false);
				final VerticalPanel vp = new VerticalPanel();
				LoaderIcon loader = new LoaderIcon("Loading batch(es) from PerformFish service, please wait...");
				vp.add(loader);
				loader.show(true);
				modal.add(vp);
				PerformFishAnalyticsServiceAsync.Util.getInstance().submitRequestToPerformFishService(mapParameters,
						new AsyncCallback<PerformFishResponse>() {

							@Override
							public void onFailure(Throwable caught) {
								modal.hide();
								Window.alert(caught.getMessage());
							}

							@Override
							public void onSuccess(PerformFishResponse performFishResponse) {
								modal.hide();
								viewController.managePerformFishServiceResponse(performFishResponse, mapParameters,
										POPULATION_LEVEL.BATCH);
							}
						});
				modal.show();

			}
		});

		eventBus.addHandler(PerformFishFieldFormChangedEvent.TYPE, new PerformFishFieldFormChangedEventHandler() {

			@Override
			public void onFieldFormChanged(PerformFishFieldFormChangedEvent performFishFieldFormChangedEvent) {

				viewController.setReloadPerformFishServiceData(true);
				boolean isValidForm = viewController.validatePerformFishInputFields();

				viewController.resetBatchIdStatus();

				if (isValidForm) {
					// viewController.resetBatchIdStatus();
					viewController.setBatchIdStatus(ControlGroupType.INFO);
					viewController.enableLoadBatches(true);
					viewController.enableSynopticTable(false);
					viewController.showAlertForLoadBatches(
							"Please load your batches corresponding to the selected options, by pressing the 'Load Batches' button",
							AlertType.INFO, false);
					viewController.enableAllAlgorithmsForSubmit(false);

				} else {
					viewController.enableLoadBatches(false);
					viewController.enableSynopticTable(false);
					viewController.enableAllAlgorithmsForSubmit(false);
				}
			}
		});

		eventBus.addHandler(AddedBatchIdEvent.TYPE, new AddedBatchIdEventHandler() {

			@Override
			public void onAddedBatchId(AddedBatchIdEvent checkValidBatchIdEvent) {

				boolean isBatchIdValid = viewController.validateBatchIdSelection();
				if (isBatchIdValid) {
					viewController.enableAllAlgorithmsForSubmit(true);
					viewController.enableSynopticTable(true);
				} else {
					viewController.enableAllAlgorithmsForSubmit(false);
					viewController.enableSynopticTable(false);
				}

				// viewController.resyncSelectedKPIs();
			}
		});

		eventBus.addHandler(SubmitRequestEvent.TYPE, new SubmitRequestEventHandler() {

			@Override
			public void onSubmitRequest(SubmitRequestEvent submitRequestEvent) {

				boolean isValidBatchId = viewController.validateBatchIdSelection();

				boolean isValidKPI = viewController.validateKPIFields();

				List<KPI> selectedKPI = viewController.getSelectedKPIs();
				viewController.manageAlgorithmsSubmit(selectedKPI.size());

				if (isValidBatchId && isValidKPI) {

					switch (submitRequestEvent.getChartType()) {

					case BOXPLOT:
						// UNARY
						callAlgorithm(submitRequestEvent.getChartType(), viewController.getSelectedBatchID(),
								selectedKPI, null);
						break;

					case SCATTER:
						if (selectedKPI.size() == 2)
							callAlgorithm(submitRequestEvent.getChartType(), viewController.getSelectedBatchID(),
									selectedKPI, null);
						else
							Window.alert("Something seems wrong... You must select exactly two KPIs to execute the "
									+ submitRequestEvent.getChartType());
						break;
					case CORRELATION:
						callDataMinerServiceForChartTypeCorrelation(viewController.getPerformFishResponse(),
								viewController.getRequestMapParameters());
						break;
					case SPEEDOMETER:
						// UNARY
						callAlgorithm(submitRequestEvent.getChartType(), viewController.getSelectedBatchID(),
								selectedKPI, selectedKPI);
						break;
					case DEA_ANALYSIS:
						createTabToSelectIOParametersForDeaAnalysis(viewController.getPerformFishResponse(),
								viewController.getRequestMapParameters());
						break;
					case DEA_CHART:
						break;
					case PERFORMFISH_SYNOPTICTABLE_BATCH:
						break;
					case PERFORMFISH_SYNOPTICTABLE_BATCH_HATCHERY:
						break;
					case PERFORMFISH_SYNOPTICTABLE_BATCH_PREGROW:
						break;
					case PERFORMFISH_SYNOPTIC_TABLE_FARM:
						break;
					default:
						break;
					}
				}

				return;
			}
		});

		eventBus.addHandler(LoadSynopticTableEvent.TYPE, new LoadSynopticTableEventHandler() {

			@Override
			public void onLoadSynopticTable(LoadSynopticTableEvent loadSynopticTableEvent) {

				boolean isValidBatchId = viewController.validateBatchIdSelection();

				if (isValidBatchId) {
					callAlgorithmSynopticTableBatch(viewController.getPerformFishResponse(),
							viewController.getRequestMapParameters());
				}

			}
		});

	}

	/**
	 * Call algorithm synoptic table batch.
	 *
	 * @param performFishResponse
	 *            the perform fish response
	 * @param performFishRequestParameters
	 *            the perform fish request parameters
	 */
	private void callAlgorithmSynopticTableBatch(final PerformFishResponse performFishResponse,
			final Map<String, List<String>> performFishRequestParameters) {
		GWT.log("Read perform fish response: " + performFishResponse);

		requestId++;
		@SuppressWarnings("unused")
		SynopticTablePanelResult synopticTablePanelResult = new SynopticTablePanelResult(requestId, viewController,
				performFishResponse, performFishRequestParameters);

	}

	//
	private void createTabToSelectIOParametersForDeaAnalysis(final PerformFishResponse performFishResponse,
			final Map<String, List<String>> performFishRequestParameters) {

		GWT.log("Read perform fish response: " + performFishResponse);

		String batchTableURL = performFishResponse.getMapParameters().get(PerformFishAnalyticsConstant.BATCHES_TABLE);

		if (batchTableURL == null || batchTableURL.isEmpty())
			Window.alert("Something seems wrong. No batches tables matching with parameter "
					+ PerformFishAnalyticsConstant.BATCHES_TABLE + " returned from service");

		final Map<String, List<String>> mapParameters = new HashMap<String, List<String>>();

		StringBuilder dataInputsFormatter = new StringBuilder();

		dataInputsFormatter
				.append(PerformFishAnalyticsConstant.DM_SCALEP_PARAM + "=" + viewController.getForm().getLevel() + ";");
		String theBatchType = viewController.getForm().getBatchType();
		theBatchType = theBatchType.replace("_CLOSED_BATCHES", ""); // REMOVING
																	// SUFFIX
																	// _CLOSED_BATCHES
																	// FOR
																	// DATAMINER
																	// CALL
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_BATCHTYPE_PARAM + "=" + theBatchType + ";");
		dataInputsFormatter
				.append(PerformFishAnalyticsConstant.DM_CHARTTYPE_PARAM + "=" + DataMinerAlgorithms.CORRELATION + ";");
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_FARMFILE_PARAM + "=" + batchTableURL + ";");
		dataInputsFormatter
				.append(PerformFishAnalyticsConstant.DM_FOCUS_PARAM + "=" + viewController.getSelectedBatchID() + ";");

		String kpiCodes = "";
		for (KPI kpi : viewController.getSelectedKPIs()) {
			kpiCodes += kpi.getCode() + "|";
		}
		// remove last |
		kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);
		dataInputsFormatter.append("inputKPI=" + kpiCodes + ";");
		// dataInputsFormatter.append("outputKPI=;");

		String dataInParameters = dataInputsFormatter.toString();
		GWT.log("Calling DM service with client input parameters: " + dataInParameters);

		mapParameters.put(PerformFishAnalyticsConstant.DATA_INPUTS, Arrays.asList(dataInParameters));

		final SubmitRequestPanel submitRequestPanel = new SubmitRequestPanel("", 1);
		submitRequestPanel.showLoader(false, null);

		String tabTitle = DataMinerAlgorithms.DEA_ANALYSIS.getTitle();
		final Tab tab = viewController.createTab(tabTitle + " #" + (viewController.currentNumberOfTab() + 1),
				PerformFishResources.INSTANCE.batch_DEAANALYSIS().getText(), submitRequestPanel);
		viewController.noSpinner(tab);
		final List<KPI> selectedKPI = new ArrayList<KPI>(viewController.getSelectedKPIs());

		DeaPanelResult deaPanelResult = new DeaPanelResult();
		deaPanelResult.addSelectedKPIs(selectedKPI);
		deaPanelResult.setDeaDescription(PerformFishResources.INSTANCE.batch_DEAANALYSIS().getText());
		deaPanelResult.addParameters(PerformFishAnalyticsConstant.DATA_INPUTS, mapParameters,
				viewController.getForm().getBatchType());
		submitRequestPanel.addWidget(deaPanelResult);
	}

	/**
	 * Call data miner service for chart type correlation.
	 *
	 * @param performFishResponse
	 *            the perform fish response
	 * @param performFishRequestParameters
	 *            the perform fish request parameters
	 */
	private void callDataMinerServiceForChartTypeCorrelation(final PerformFishResponse performFishResponse,
			final Map<String, List<String>> performFishRequestParameters) {

		GWT.log("Read perform fish response: " + performFishResponse);

		String batchTableURL = performFishResponse.getMapParameters().get(PerformFishAnalyticsConstant.BATCHES_TABLE);

		if (batchTableURL == null || batchTableURL.isEmpty())
			Window.alert("Something seems wrong. No batches tables matching with parameter "
					+ PerformFishAnalyticsConstant.BATCHES_TABLE + " returned from service");

		final Map<String, List<String>> mapParameters = new HashMap<String, List<String>>();

		StringBuilder dataInputsFormatter = new StringBuilder();

		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_SCALEP_PARAM + "=BATCH;");
		String theBatchType = viewController.getForm().getBatchType();
		theBatchType = theBatchType.replace("_CLOSED_BATCHES", ""); // REMOVING
																	// SUFFIX
																	// _CLOSED_BATCHES
																	// FOR
																	// DATAMINER
																	// CALL
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_BATCHTYPE_PARAM + "=" + theBatchType + ";");
		dataInputsFormatter
				.append(PerformFishAnalyticsConstant.DM_CHARTTYPE_PARAM + "=" + DataMinerAlgorithms.CORRELATION + ";");
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_FARMFILE_PARAM + "=" + batchTableURL + ";");
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_FOCUS_PARAM + "=ID;");

		String kpiCodes = "";
		for (KPI kpi : viewController.getSelectedKPIs()) {
			kpiCodes += kpi.getCode() + "|";
		}
		// remove last |
		kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);
		dataInputsFormatter.append("inputKPI=" + kpiCodes + ";");
		// dataInputsFormatter.append("outputKPI=;");

		String dataInParameters = dataInputsFormatter.toString();
		GWT.log("Calling DM service with client input parameters: " + dataInParameters);

		mapParameters.put(PerformFishAnalyticsConstant.DATA_INPUTS, Arrays.asList(dataInParameters));

		final SubmitRequestPanel submitRequestPanel = new SubmitRequestPanel("", 1);
		submitRequestPanel.showLoader(true,
				"Submitting " + DataMinerAlgorithms.CORRELATION.getTitle() + " request to DM Service...");

		final String tabTitle = DataMinerAlgorithms.CORRELATION.getTitle();
		final String tabDescr = PerformFishResources.INSTANCE.batch_CORRELATION().getText();
		final Tab tab = viewController.createTab(tabTitle + " #" + (viewController.currentNumberOfTab() + 1),
				 tabDescr, submitRequestPanel);

		final List<KPI> selectedKPI = new ArrayList<KPI>(viewController.getSelectedKPIs());
		final List<String> batchIDs = new ArrayList<String>(viewController.getListBatchesID());
		
		requestId++;
		PerformFishAnalyticsServiceAsync.Util.getInstance().callingDataMinerPerformFishCorrelationAnalysis(
				performFishResponse, mapParameters, new AsyncCallback<DataMinerResponse>() {

					@Override
					public void onSuccess(DataMinerResponse dmResponse) {
						submitRequestPanel.showLoader(false, null);
						checkTabSpinner(submitRequestPanel, tab);
						CorrelationPanelResult analyticsPanelResult = new CorrelationPanelResult(requestId,tabTitle, tabDescr);
						analyticsPanelResult.addSelectedAreas(
								performFishRequestParameters.get(PerformFishAnalyticsConstant.PERFORM_FISH_AREA_PARAM));
						analyticsPanelResult.addSelectedKPIs(selectedKPI);
						analyticsPanelResult.addListFocusIds(batchIDs);
						analyticsPanelResult.addParameters(PerformFishAnalyticsConstant.DATA_INPUTS, mapParameters,
								viewController.getForm().getBatchType());
						analyticsPanelResult.addResults(dmResponse);
						submitRequestPanel.addWidget(analyticsPanelResult);
						// viewController.geTabPanelView().addResult(resultPanel,
						// "Analysis
						// #"+(viewController.geTabPanelView().countTab()+1));
						// modal.hide();
					}

					@Override
					public void onFailure(Throwable caught) {
						GWT.log(caught.toString());
						submitRequestPanel.showLoader(false, null);
						checkTabSpinner(submitRequestPanel, tab);
						try {
							CorrelationPanelResult analyticsPanelResult = new CorrelationPanelResult(requestId, tabTitle, tabDescr);
							analyticsPanelResult.addSelectedAreas(performFishRequestParameters
									.get(PerformFishAnalyticsConstant.PERFORM_FISH_AREA_PARAM));
							analyticsPanelResult.addSelectedKPIs(selectedKPI);
							analyticsPanelResult.addListFocusIds(batchIDs);
							analyticsPanelResult.addParameters(PerformFishAnalyticsConstant.DATA_INPUTS, mapParameters,
									viewController.getForm().getBatchType());
							submitRequestPanel.addWidget(analyticsPanelResult);
						} catch (Exception e) {
							GWT.log("Error in AnalyticsPanelResult: " + e.getLocalizedMessage(), e);
						}

						Alert error = new Alert(caught.getMessage());
						error.setClose(false);
						error.setType(AlertType.ERROR);
						submitRequestPanel.addWidget(error);
					}
				});
	}

	/**
	 * Creating new TAB and calling DM algorithm.
	 *
	 * @param algorithm
	 *            the algorithm
	 * @param focusID
	 *            the focus id
	 * @param inputKPI
	 *            the input kpi
	 * @param outputKPI
	 *            the output kpi
	 */
	private void callAlgorithm(final DataMinerAlgorithms algorithm, String focusID, List<KPI> inputKPI,
			final List<KPI> outputKPI) {

		String selectedBatchID = viewController.getSelectedBatchID();

		if (selectedBatchID == null || selectedBatchID.isEmpty())
			Window.alert("Something seems wrong, no selected BatchID, try again");
		
		requestId++;
		switch (algorithm) {
		case BOXPLOT:
			@SuppressWarnings("unused")
			BoxPlotPanelResult boxPlotPanelResult = new BoxPlotPanelResult(requestId, viewController, algorithm,
					focusID, inputKPI, outputKPI);
			break;
		case SPEEDOMETER:
			@SuppressWarnings("unused")
			SpeedometerPanelResult speedometerPanelResult = new SpeedometerPanelResult(requestId, viewController,
					algorithm, focusID, inputKPI, outputKPI);

			break;
		case SCATTER:
			@SuppressWarnings("unused")
			ScatterPanelResult scatterPanelResult = new ScatterPanelResult(requestId, viewController, algorithm,
					focusID, inputKPI, outputKPI);
			break;
		default:
			break;
		// callDataMinerServiceForChart(viewController.getPerformFishResponse(),
		// POPULATION_LEVEL.BATCH, inputKPI,
		// outputKPI, algorithm, focusID, submitRequestPanel,
		// submitRequestPanel.getContainerPanel(), tab,
		// req, oIteration);
		}

	}

	/**
	 * Remove the spinner if all DM responses are returned.
	 *
	 * @param requestPanel
	 *            the request panel
	 * @param tab
	 *            the tab
	 */
	private void checkTabSpinner(SubmitRequestPanel requestPanel, Tab tab) {
		requestPanel.incrementCompletedRequests();
		int completed = requestPanel.getCompletedRequests();
		int total = requestPanel.getTotalRequests();

		if (completed >= total) {
			viewController.noSpinner(tab);
		}
	}

}
