package org.gcube.portlets.user.performfishanalytics.server.util.database.data;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant;
import org.gcube.portlets.user.performfishanalytics.shared.PopulationType;
import org.gcube.portlets.user.performfishanalytics.shared.Year;

/**
 * The Class YearData.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         May 6, 2019
 */
public class YearData {

	/**
	 * Gets the list years.
	 *
	 * @param batchType
	 *            the batch type
	 * @return the list years
	 */
	public static ArrayList<Year> getListYears(PopulationType batchType) {
		
		List<Year> listSpecies = new ArrayList<Year>();
		
		if (batchType != null) {
			Year a2016=new Year(java.util.UUID.randomUUID().toString(), "2016", batchType);
			Year a2017=new Year(java.util.UUID.randomUUID().toString(), "2017", batchType);
			Year a2018=new Year(java.util.UUID.randomUUID().toString(), "2018", batchType);
			Year a2019=new Year(java.util.UUID.randomUUID().toString(), "2019", batchType);
			Year a2020=new Year(java.util.UUID.randomUUID().toString(), "2020", batchType);
			Year a2021=new Year(java.util.UUID.randomUUID().toString(), "2021", batchType);
			Year a2022=new Year(java.util.UUID.randomUUID().toString(), "2022", batchType);
			
			if (batchType.getName()
					.equals(PerformFishAnalyticsConstant.BATCH_LEVEL.GROW_OUT_AGGREGATED_CLOSED_BATCHES.name())) {
				listSpecies.add(a2016);
				listSpecies.add(a2017);
				listSpecies.add(a2018);
				
			} else {
				if (batchType.getName().equals(PerformFishAnalyticsConstant.BATCH_LEVEL.GROW_OUT_AGGREGATED.name())) {
					listSpecies.add(a2016);
					listSpecies.add(a2017);
					listSpecies.add(a2018);
					listSpecies.add(a2019);
					listSpecies.add(a2020);
					listSpecies.add(a2021);
					listSpecies.add(a2022);
				} else {
				}
			}

		}
		return (ArrayList<Year>) listSpecies;
	}

}
