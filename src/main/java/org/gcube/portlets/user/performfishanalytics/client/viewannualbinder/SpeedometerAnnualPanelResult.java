package org.gcube.portlets.user.performfishanalytics.client.viewannualbinder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.user.performfishanalytics.client.DataMinerAlgorithms;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsServiceAsync;
import org.gcube.portlets.user.performfishanalytics.client.annualcontrollers.DataMinerInputParameters;
import org.gcube.portlets.user.performfishanalytics.client.annualcontrollers.PerformFishAnnualAnalyticsViewController;
import org.gcube.portlets.user.performfishanalytics.client.resources.PerformFishResources;
import org.gcube.portlets.user.performfishanalytics.client.view.LoaderIcon;
import org.gcube.portlets.user.performfishanalytics.client.viewbinder.ShowResult;
import org.gcube.portlets.user.performfishanalytics.client.viewbinder.SubmitRequestPanel;
import org.gcube.portlets.user.performfishanalytics.shared.FileContentType;
import org.gcube.portlets.user.performfishanalytics.shared.KPI;
import org.gcube.portlets.user.performfishanalytics.shared.OutputFile;
import org.gcube.portlets.user.performfishanalytics.shared.csv.CSVFile;
import org.gcube.portlets.user.performfishanalytics.shared.dataminer.DataMinerResponse;
import org.gcube.portlets.user.performfishanalytics.shared.performfishservice.PerformFishResponse;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Panel;

public class SpeedometerAnnualPanelResult {

	private Map<Integer, FlexTable> boxPlotOrderBy = new HashMap<Integer, FlexTable>();

	private PerformFishAnnualAnalyticsViewController viewAnnualController;
	private int requestId = 0;
	private int outputIteration = 0;
	private int outputNumber = 1;

	private Button savePDFButton;

	public SpeedometerAnnualPanelResult(int reqId, PerformFishAnnualAnalyticsViewController viewAnnualController,
			PerformFishResponse thePerformFishResponse, final DataMinerAlgorithms algorithm, String focusID,
			List<KPI> inputKPI, final List<KPI> outputKPI) {
		GWT.log("RequestID: " + reqId);
		requestId = reqId;
		this.viewAnnualController = viewAnnualController;
		TextResource algDescr = PerformFishResources.INSTANCE.farm_SPEEDOMETER();

		String algDesrTxt = algDescr != null ? algDescr.getText() : null;

		// TODO
		final String tabTitle = algorithm.getTitle();
		final String tabDescr = algDesrTxt;

		final SubmitRequestPanel submitRequestPanel = new SubmitRequestPanel("", 1);
		Tab tab = viewAnnualController.createTab(tabTitle + " #" + (viewAnnualController.currentNumberOfTab() + 1),
				algDesrTxt, submitRequestPanel);

		HorizontalPanel res = new HorizontalPanel();

		savePDFButton = new Button();
		savePDFButton.getElement().getStyle().setMargin(10, Unit.PX);
		savePDFButton.setIcon(IconType.PRINT);
		savePDFButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				saveFilePDF(algorithm.getId(), tabTitle, tabTitle, tabDescr, requestId, outputIteration, outputNumber);
				// dmResponse.getListOutput().size()
			}
		});
		savePDFButton.setVisible(false);
		
		res.add(savePDFButton);
		submitRequestPanel.addWidget(res);
		int oIteration = 0;

		submitRequestPanel.setTotalRequests(inputKPI.size());
		for (KPI kpi : inputKPI) {
			oIteration++;
			String title = "KPI: " + kpi.getName();
			HTML toBigTitle = new HTML(title);
			toBigTitle.getElement().setId("KPI_SpeedometerAnnual_" + requestId + "_" + oIteration);
			toBigTitle.getElement().addClassName("to-big-title");
			submitRequestPanel.addWidget(toBigTitle);
			HorizontalPanel hp = new HorizontalPanel();
			hp.getElement().addClassName("ext-horizontal-panel");

			DataMinerInputParameters dmInputParams = new DataMinerInputParameters(thePerformFishResponse,
					viewAnnualController.getSelectedYears(), Arrays.asList(kpi), null, algorithm, focusID);
			callDataMinerServiceForChart(dmInputParams, submitRequestPanel, hp, tab, oIteration);
			// resultPanel.add(hp);
			submitRequestPanel.addWidget(hp);
		}
		outputIteration = oIteration;

	}

	/**
	 * Call data miner service for chart.
	 *
	 * @param dmInputParameters
	 *            the dm input parameters
	 * @param requestPanel
	 *            the request panel
	 * @param panelContainer
	 *            the panel
	 * @param tab
	 *            the tab
	 * @param tabDescr
	 * @param tabTitle
	 */
	private void callDataMinerServiceForChart(final DataMinerInputParameters dmInputParameters,
			final SubmitRequestPanel requestPanel, final ComplexPanel panelContainer, final Tab tab,
			final int oIteration) {

		GWT.log("Building DM request with input parameters: " + dmInputParameters);

		StringBuilder dataInputsFormatter = new StringBuilder();
		dataInputsFormatter
				.append(PerformFishAnalyticsConstant.DM_CHARTTYPE_PARAM + "=" + dmInputParameters.getChartType() + ";");

		String yearsValue = "";
		for (String year : dmInputParameters.getSelectedYears()) {
			yearsValue += year + "|";
		}
		yearsValue = yearsValue.substring(0, yearsValue.length() - 1);

		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_YEARS_PARAM + "=" + yearsValue + ";");
		dataInputsFormatter
				.append(PerformFishAnalyticsConstant.DM_FOCUS_PARAM + "=" + dmInputParameters.getFocusID() + ";");
		// dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_FOCUS_PARAM+"=ID;");

		if (dmInputParameters.getInputKPI() != null && dmInputParameters.getInputKPI().size() > 0) {
			String kpiCodes = "";
			for (KPI kpi : dmInputParameters.getInputKPI()) {
				kpiCodes += kpi.getCode() + "|";
			}
			// remove last |
			kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);

			GWT.log("Input KPICodes: " + kpiCodes);
			// ADDING KPIs code
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_INPUT_KPI_PARAM + "=" + kpiCodes + ";");

		}

		if (dmInputParameters.getOutputKPI() != null && dmInputParameters.getOutputKPI().size() > 0) {
			String kpiCodes = "";
			for (KPI kpi : dmInputParameters.getOutputKPI()) {
				kpiCodes += kpi.getCode() + "|";
			}
			// remove last |
			kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);

			GWT.log("Output KPICodes: " + kpiCodes);
			// ADDING KPIs code
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_OUTPUT_KPI_PARAM + "=" + kpiCodes + ";");
		}

		Map<String, String> performFishResponseMap = dmInputParameters.getPerformFishResponse().getMapParameters();

		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.LethalIncidentsTable);
		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.AnnualTable);
		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.AntibioticsTable);
		dataInputsFormatter = appendDMInputTable(performFishResponseMap, dataInputsFormatter,
				PerformFishAnalyticsConstant.PFSERVICE_TO_DM_MAPPING_TABLE.AntiparasiticTable);

		String dataInParameters = dataInputsFormatter.toString();
		GWT.log("Calling DM service with client input parameters: " + dataInParameters);

		Map<String, List<String>> mapParameters = new HashMap<String, List<String>>();
		mapParameters.put(PerformFishAnalyticsConstant.DATA_INPUTS, Arrays.asList(dataInParameters));

		final LoaderIcon loaderIcon = new LoaderIcon(
				"Submitting request to " + dmInputParameters.getChartType().getTitle() + " Analysis...");
		loaderIcon.setVisible(true);
		panelContainer.setVisible(true);
		panelContainer.add(loaderIcon);
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			@Override
			public void execute() {
				loaderIcon.setFocus(true);
			}
		});

		PerformFishAnalyticsServiceAsync.Util.getInstance().callingDataMinerPerformFishAnnualAnalysis(mapParameters,
				new AsyncCallback<DataMinerResponse>() {

					@Override
					public void onSuccess(DataMinerResponse dmResponse) {
						loaderIcon.setVisible(false);
						checkTabSpinner(requestPanel, tab);
						// field_unary_algorithm.setVisible(true);
						GWT.log("I'm displaying: " + dmResponse);
						displayOutputFilesAsStaticEntities(dmResponse, dmInputParameters.getChartType(),
								dmInputParameters.getInputKPI(), dmInputParameters.getOutputKPI(),
								dmInputParameters.getFocusID(), panelContainer, false, oIteration);
					}

					@Override
					public void onFailure(Throwable caught) {
						loaderIcon.setVisible(false);
						checkTabSpinner(requestPanel, tab);
						displayOutputFilesAsStaticEntities(null, dmInputParameters.getChartType(),
								dmInputParameters.getInputKPI(), dmInputParameters.getOutputKPI(),
								dmInputParameters.getFocusID(), panelContainer, true, oIteration);

					}
				});
	}

	/**
	 * Append DM input table.
	 *
	 * @param performFishResponseMap
	 *            the perform fish response map
	 * @param dataInputsFormatter
	 *            the data inputs formatter
	 * @param table
	 *            the table
	 * @return the string builder
	 */
	private StringBuilder appendDMInputTable(Map<String, String> performFishResponseMap,
			StringBuilder dataInputsFormatter, PFSERVICE_TO_DM_MAPPING_TABLE table) {

		String toDMInputTable = performFishResponseMap.get(table.getPerformFishTable());

		if (toDMInputTable != null && !toDMInputTable.isEmpty()) {
			dataInputsFormatter.append(table.getDataMinerTable() + "=" + toDMInputTable + ";");
		}

		return dataInputsFormatter;
	}

	private String getOutputId(int oIteration, String title) {
		String outputId = "OutputId_" + requestId + "_" + oIteration + "_" + 1;
		return outputId;

	}

	private void displayOutputFilesAsStaticEntities(final DataMinerResponse dmResponse,
			final DataMinerAlgorithms chartType, List<KPI> inputKPIs, List<KPI> outputKPIs, final String focusID,
			final Panel container, boolean displayError, final int oIteration) {

		String title = displayError ? "No results " : "";

		if (displayError) {
			Alert alert = new Alert(title);
			alert.setType(AlertType.ERROR);
			alert.setClose(false);
			alert.getElement().getStyle().setMargin(10, Unit.PX);
			container.add(alert);
			return;
		}

		savePDFButton.setVisible(true);
		
		final String toTitle = title;

		for (OutputFile outputFile : dmResponse.getListOutput()) {
			final FileContentType fileContentType = outputFile.getDataType();

			switch (outputFile.getDataType()) {
			case IMAGE:
				PerformFishAnalyticsServiceAsync.Util.getInstance().getImageFile(outputFile,
						new AsyncCallback<String>() {

							@Override
							public void onFailure(Throwable caught) {
								// showAlert(caught.getMessage(),
								// AlertType.ERROR, true,
								// uib_vp_deanalanlysis_request_container);
								Window.alert(caught.getMessage());

							}

							@Override
							public void onSuccess(String base64Content) {

								String title = toTitle;

								title = chartType.getTitle() + "<br>";
								title += "Selected Farm: " + focusID + "<br>";
								title += "Normalized with respect to all farm data in the VRE";

								String outputId = getOutputId(oIteration, title);
								ShowResult showResult = new ShowResult(outputId, title, fileContentType);
								showResult.showImage(base64Content);
								container.add(showResult);

							}
						});
				break;
			case CSV:
				PerformFishAnalyticsServiceAsync.Util.getInstance().getCSVFile(outputFile, true,
						new AsyncCallback<CSVFile>() {

							@Override
							public void onFailure(Throwable caught) {
								// showAlert(caught.getMessage(),
								// AlertType.ERROR, true,
								// uib_vp_deanalanlysis_request_container);
								Window.alert(caught.getMessage());
							}

							@Override
							public void onSuccess(CSVFile result) {
								GWT.log("Displaying: " + result);

								String cssTableStyle = "simpletable";

								String title = toTitle;

								String outputId = getOutputId(oIteration, title);
								ShowResult showResult = new ShowResult(outputId, title, fileContentType);
								showResult.showCSVFile(result, cssTableStyle);
								container.add(showResult);

							}
						});
				break;

			default:
				break;
			}

		}

	}

	private void displayingOrderedBoxPlot(final DataMinerResponse dmResponse, String title, final Panel container,
			ShowResult showResult) {
		int hashcode = dmResponse.hashCode();
		GWT.log("The hascode is: " + hashcode);
		FlexTable flex = boxPlotOrderBy.get(hashcode);

		if (flex == null) {
			GWT.log("The flextable is null");
			flex = new FlexTable();
			boxPlotOrderBy.put(hashcode, flex);
			container.add(flex);
		}

		if (title.startsWith("My")) {
			flex.setWidget(0, 0, showResult);
		} else if (title.startsWith("All")) {
			flex.setWidget(0, 1, showResult);
		} else {
			flex.setWidget(0, 2, showResult);
		}
	}

	/**
	 * Remove the spinner if all DM responses are returned.
	 *
	 * @param requestPanel
	 *            the request panel
	 * @param tab
	 *            the tab
	 */
	private void checkTabSpinner(SubmitRequestPanel requestPanel, Tab tab) {
		requestPanel.incrementCompletedRequests();
		int completed = requestPanel.getCompletedRequests();
		int total = requestPanel.getTotalRequests();

		if (completed >= total) {
			viewAnnualController.noSpinner(tab);
		}
	}

	private static native void saveFilePDF(String chartType, String filename, String tabTitle, String tabDescr,
			int requestId, int outputIteration, int outputNumber)/*-{
		var that = this;
		console.log('saveFilePDF()');
		console.log('requestId: ' + requestId);
		console.log('OutputIteration: ' + outputIteration);
		console.log('OutputNumber:' + outputNumber);

		var tTitle = tabTitle;
		var tDescr = tabDescr;

		var pdoc = new jsPDF("p", "mm", "a4");
		pdoc.setProperties({
			title : 'PerformFish ' + tTitle,
			subject : ' Results',
			author : 'PerformFish',
			keywords : 'PerformFish',
			creator : 'D4Science'
		});

		var lMargin = 15; //left margin in mm
		var rMargin = 15; //right margin in mm
		var tMargin = 15; //top margin in mm
		var bMargin = 15; //bottom margin in mm
		var pdfWidthInMM = 210; // width of A4 in mm
		var pdfHeightInMM = 297; // height of A4 in mm
		var pageCenter = pdfWidthInMM / 2;

		pdoc.setFontSize(24);
		var title = "PerformFish " + tTitle;
		var titleHeight = pdoc.getLineHeight(title) / pdoc.internal.scaleFactor
		var xPos = lMargin;
		var yPos = tMargin;
		pdoc.text(title, pageCenter, yPos, 'center');
		yPos += titleHeight;

		pdoc.setFontSize(10);
		var lineHeight = pdoc.getLineHeight(tDescr) / pdoc.internal.scaleFactor
		var splittedAnalysisDescription = pdoc.splitTextToSize(tDescr,
				(pdfWidthInMM - lMargin - rMargin));
		var lines = splittedAnalysisDescription.length // splitted text is a string array
		var analysisDescriptionHeight = lines * lineHeight

		pdoc.text(splittedAnalysisDescription, xPos, yPos, 'left');
		yPos += analysisDescriptionHeight;

		//yPos += 2;
		//pdoc.text("Analysis:", xPos, yPos, 'left');
		yPos += 6;

		for (var i = 1; i <= outputIteration; i++) {

			var kpiId = 'KPI_SpeedometerAnnual_' + requestId + '_' + i;
			console.log('kpiId: ' + kpiId);

			var kpiName = $doc.getElementById(kpiId);

			yPos += 2;
			pdoc.setFontSize(14);
			pdoc.setFontType("bold");

			var kpiText = kpiName.textContent;
			var kpiHeight = pdoc.getLineHeight(kpiText)
					/ pdoc.internal.scaleFactor;
			var splittedKpiName = pdoc.splitTextToSize(kpiText, (pdfWidthInMM
					- lMargin - rMargin));
			var kpiLines = splittedKpiName.length; // splitted text is a string array
			var kpiLinesHeight = kpiLines * kpiHeight;
			pdoc.text(splittedKpiName, xPos, yPos, 'left');
			yPos += kpiLinesHeight;
			yPos += 6;

			pdoc.setFontSize(10);
			pdoc.setFontType("normal");


			for (var j = 1; j <= outputNumber; j++) {

				var outputId = 'OutputId_' + requestId + '_' + i + '_' + j;
				console.log('OutputId: ' + outputId);

				var resultOutputNumber = $doc.getElementById(outputId);

				console.log('OutputId_: ' + resultOutputNumber.innerHTML);
				var resultType = resultOutputNumber.className;

				console.log('resultOutputNumber className: ' + resultType);

				if (typeof resultType !== 'undefined' && resultType !== null
						&& resultType !== '') {

					if (resultType == 'csv') {
						console.log('Result Type csv: ' + i);
						// foo could get resolved and it's defined

						var childrenTable = resultOutputNumber.children;
						var titleCurrentTable = childrenTable[0].rows[0].cells[0];
						console.log('Title current table: '
								+ titleCurrentTable.textContent);
						yPos += 2;
						pdoc.setFontSize(10);
						pdoc.setFontType("bold");
						pdoc.text(titleCurrentTable.textContent, xPos, yPos,
								'left');
						yPos += 6;

						pdoc.setFontSize(10);
						pdoc.setFontType("normal");

						var secondDiv = childrenTable[0].rows[1].cells[0];
						var secondTable = secondDiv.children[0].children[0];

						console.log('CSV SecondTable: ' + secondTable);

						pdoc.autoTable({
							theme : 'grid',
							startY : yPos,
							pageBreak : 'auto',
							bodyStyles : {
								fontSize : 9
							},
							html : secondTable
						});

						yPos = pdoc.previousAutoTable.finalY + 6;
					} else {
						if (resultType == 'image') {
							console.log('Result Type image: ' + i);
							var childrenTable = resultOutputNumber.children;

							var titleCurrentImage = childrenTable[0].rows[0].cells[0];
							console.log('Title current image: '
									+ titleCurrentImage.textContent);
							yPos += 2;
							pdoc.setFontSize(10);
							pdoc.setFontType("bold");
							pdoc.text(titleCurrentImage.textContent, xPos,
									yPos, 'left');
							yPos += 6;

							pdoc.setFontSize(10);
							pdoc.setFontType("normal");

							var secondDiv = childrenTable[0].rows[1].cells[0];
							var imageElement = secondDiv.getElementsByTagName(
									'img').item(0);
							console.log('Image element: ' + imageElement);
							pdoc.addImage(imageElement, lMargin, yPos);
							console.log('Image Height: ' + imageElement.height);
							yPos += (imageElement.height * 0.26458333);

						} else {
							console.log('Result Type unknow: ' + i);
						}

					}
				} else {
					console.log('Result Type is undefined:' + i);
				}

			}

			if (i == outputIteration) {

			} else {
				if (pdfHeightInMM - yPos < pdfHeightInMM / 4) {
					pdoc.addPage();
					yPos = tMargin; // Restart position
				}
			}

		}

		var numberOfPages=pdoc.internal.getNumberOfPages()
		console.log('NumberOfPages: ' + numberOfPages);
		for (var k = 1; k <= numberOfPages; k++) {
			pdoc.setPage(k);
			console.log('CurrentPage: ' + k);
			var footner = 'Page ' + k + "/"+numberOfPages;
			pdoc.text(footner, pageCenter, pdfHeightInMM - 7, 'center');
			
		}
		
		console.log('Done');

		pdoc.save(filename);

	}-*/;

}
