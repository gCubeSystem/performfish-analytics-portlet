/**
 *
 */
package org.gcube.portlets.user.performfishanalytics.client.viewbinder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.user.performfishanalytics.client.DataMinerAlgorithms;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsServiceAsync;
import org.gcube.portlets.user.performfishanalytics.client.view.LoaderIcon;
import org.gcube.portlets.user.performfishanalytics.shared.KPI;
import org.gcube.portlets.user.performfishanalytics.shared.OutputFile;
import org.gcube.portlets.user.performfishanalytics.shared.csv.CSVFile;
import org.gcube.portlets.user.performfishanalytics.shared.dataminer.DataMinerResponse;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class AnalyticsPanelResult.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jan 29, 2019
 */
public class DeaPanelResult extends Composite {

	private static final String RESULTID = "DeaAnalysisResult";

	private static DeaPanelResultUiBinder uiBinder = GWT.create(DeaPanelResultUiBinder.class);

	/**
	 * The Interface AnalyticsPanelResultUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR
	 *         (francesco.mangiacrapa@isti.cnr.it) Jan 30, 2019
	 */
	interface DeaPanelResultUiBinder extends UiBinder<Widget, DeaPanelResult> {
	}

	@UiField
	HTMLPanel field_html_panel;

	@UiField
	VerticalPanel field_parameters_container;

	// @UiField
	// Label uib_label_focus_id;

	// @UiField
	// ControlGroup cg_focus_id_correlation;

	// @UiField
	// ListBox field_list_focus_id_correlation;

	@UiField
	ComplexPanel alert_info;

	@UiField
	ComplexPanel alert_error;

	@UiField
	ControlGroup cg_list_dea_kpi;

	@UiField
	ControlGroup cg_list_dea_input_kpi;

	@UiField
	ControlGroup cg_list_dea_output_kpi;

	@UiField
	ListBox list_dea_kpi;

	@UiField
	ListBox list_dea_input_kpi;

	@UiField
	ListBox list_dea_output_kpi;

	@UiField
	HorizontalPanel uib_vp_deanalanlysis_request_container;

	@UiField
	VerticalPanel uib_vp_deanalanlysis_algorithm;

	@UiField
	VerticalPanel uib_vp_deanalanlysis_algorithm_container;

	@UiField
	Button uib_save_pdf;

	@UiField
	Button button_dea_analys_request;

	@UiField
	Button uib_add_to_input_kpi;
	@UiField
	Button uib_remove_from_input_kpi;
	@UiField
	Button uib_add_to_output_kpi;
	@UiField
	Button uib_remove_from_output_kpi;

	@UiField
	Button uib_reset_input_kpi;

	@UiField
	Button uib_reset_output_kpi;

	private enum KpiButtonType {
		KPI, INPUT_KPI, OUTPUT_KPI
	}

	private Map<String, String> dataInputParameters;

	private DataMinerResponse dmResponse;

	private Map<String, List<KPI>> kpiMapPointers = new HashMap<String, List<KPI>>();

	private List<KPI> selectedKPIs;

	private DeaPanelResult myDea;

	private static int callNumber;

	private static Map<String, Integer> outputNumbers = new HashMap<String, Integer>();

	private String deaDescription;

	// private List<String> selectedAreas;

	public DeaPanelResult() {
		GWT.log("DeaPanelResult");

		initWidget(uiBinder.createAndBindUi(this));

		myDea = this;

		uib_save_pdf.setIcon(IconType.PRINT);
		uib_save_pdf.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				saveFilePDF(myDea, "DeaAnalysis.pdf");
			}
		});

		button_dea_analys_request.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				alert_error.clear();
				cg_list_dea_input_kpi.setType(ControlGroupType.NONE);
				cg_list_dea_output_kpi.setType(ControlGroupType.NONE);

				if (list_dea_input_kpi.getItemCount() < 1) {
					showAlert("You must select at least one Input KPI", AlertType.ERROR, true, alert_error);
					cg_list_dea_input_kpi.setType(ControlGroupType.ERROR);
					return;
				}

				if (list_dea_output_kpi.getItemCount() < 1) {
					showAlert("You must select at least one Output KPI", AlertType.ERROR, true, alert_error);
					cg_list_dea_output_kpi.setType(ControlGroupType.ERROR);
					return;
				}

				List<String> inputKPINames = new ArrayList<String>();
				for (int i = 0; i < list_dea_input_kpi.getItemCount(); i++) {
					inputKPINames.add(list_dea_input_kpi.getItemText(i));
				}

				List<String> outputKPINames = new ArrayList<String>();
				for (int i = 0; i < list_dea_output_kpi.getItemCount(); i++) {
					outputKPINames.add(list_dea_output_kpi.getItemText(i));
				}

				callDeaAnalysis(inputKPINames, outputKPINames, button_dea_analys_request);
			}
		});

		uib_add_to_input_kpi.setIcon(IconType.BACKWARD);
		uib_add_to_input_kpi.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				addListFields(KpiButtonType.INPUT_KPI);
			}
		});

		uib_remove_from_input_kpi.setIcon(IconType.FORWARD);
		uib_remove_from_input_kpi.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				removeListFields(KpiButtonType.INPUT_KPI);
			}
		});

		uib_add_to_output_kpi.setIcon(IconType.FORWARD);
		uib_add_to_output_kpi.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				addListFields(KpiButtonType.OUTPUT_KPI);
			}
		});

		uib_remove_from_output_kpi.setIcon(IconType.BACKWARD);
		uib_remove_from_output_kpi.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				removeListFields(KpiButtonType.OUTPUT_KPI);
			}
		});

		uib_reset_input_kpi.setIcon(IconType.REMOVE_CIRCLE);
		uib_reset_input_kpi.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				resetListFields(KpiButtonType.INPUT_KPI);
			}
		});

		uib_reset_output_kpi.setIcon(IconType.REMOVE_CIRCLE);
		uib_reset_output_kpi.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				resetListFields(KpiButtonType.OUTPUT_KPI);
			}
		});

		validateKPIList();
	}

	private void validateKPIList() {
		if (list_dea_output_kpi.getItemCount() > 0 && list_dea_input_kpi.getItemCount() > 0) {
			alert_info.clear();
			alert_error.clear();
			cg_list_dea_input_kpi.setType(ControlGroupType.NONE);
			cg_list_dea_output_kpi.setType(ControlGroupType.NONE);
			button_dea_analys_request.setEnabled(true);

		} else {
			alert_info.clear();
			button_dea_analys_request.setEnabled(false);
			showAlert("Please select Input and Output KPI", AlertType.INFO, true, alert_info);
		}
	}

	private static int getOutputNumber(String callId) {
		return outputNumbers.get(callId);
	}

	/**
	 * Add to Input or Output KPI selected
	 * 
	 * @param kpiButtonType
	 *            kpi button type
	 * 
	 */
	private void addListFields(KpiButtonType kpiButtonType) {
		List<String> notSelected = null;

		switch (kpiButtonType) {
		case INPUT_KPI:
			notSelected = new ArrayList<String>();
			for (int i = 0; i < list_dea_kpi.getItemCount(); i++) {
				if (list_dea_kpi.isItemSelected(i)) {
					list_dea_input_kpi.addItem(list_dea_kpi.getItemText(i));
				} else {
					notSelected.add(list_dea_kpi.getItemText(i));
				}
			}
			list_dea_kpi.clear();
			for (int i = 0; i < notSelected.size(); i++) {
				list_dea_kpi.addItem(notSelected.get(i));
			}
			break;
		case KPI:
			break;
		case OUTPUT_KPI:
			notSelected = new ArrayList<String>();
			for (int i = 0; i < list_dea_kpi.getItemCount(); i++) {
				if (list_dea_kpi.isItemSelected(i)) {
					list_dea_output_kpi.addItem(list_dea_kpi.getItemText(i));
				} else {
					notSelected.add(list_dea_kpi.getItemText(i));
				}
			}
			list_dea_kpi.clear();
			for (int i = 0; i < notSelected.size(); i++) {
				list_dea_kpi.addItem(notSelected.get(i));
			}
			break;
		default:
			break;
		}

		validateKPIList();

	}

	/**
	 * Remove from Input or Output KPI selected
	 * 
	 * @param kpiButtonType
	 *            kpi button type
	 */
	private void removeListFields(KpiButtonType kpiButtonType) {
		List<String> notSelected = null;

		switch (kpiButtonType) {
		case INPUT_KPI:
			notSelected = new ArrayList<String>();
			for (int i = 0; i < list_dea_input_kpi.getItemCount(); i++) {
				if (list_dea_input_kpi.isItemSelected(i)) {
					list_dea_kpi.addItem(list_dea_input_kpi.getItemText(i));
				} else {
					notSelected.add(list_dea_input_kpi.getItemText(i));
				}
			}
			list_dea_input_kpi.clear();
			for (int i = 0; i < notSelected.size(); i++) {
				list_dea_input_kpi.addItem(notSelected.get(i));
			}
			break;
		case KPI:
			break;
		case OUTPUT_KPI:
			notSelected = new ArrayList<String>();
			for (int i = 0; i < list_dea_output_kpi.getItemCount(); i++) {
				if (list_dea_output_kpi.isItemSelected(i)) {
					list_dea_kpi.addItem(list_dea_output_kpi.getItemText(i));
				} else {
					notSelected.add(list_dea_output_kpi.getItemText(i));
				}
			}
			list_dea_output_kpi.clear();
			for (int i = 0; i < notSelected.size(); i++) {
				list_dea_output_kpi.addItem(notSelected.get(i));
			}
			break;
		default:
			break;
		}

		validateKPIList();

	}

	/**
	 * Reset Input or Output KPI selected
	 * 
	 * @param kpiButtonType
	 *            kpi button type
	 */
	private void resetListFields(KpiButtonType kpiButtonType) {

		switch (kpiButtonType) {
		case INPUT_KPI:
			for (int i = 0; i < list_dea_input_kpi.getItemCount(); i++) {
				list_dea_kpi.addItem(list_dea_input_kpi.getItemText(i));
			}
			list_dea_input_kpi.clear();
			break;
		case KPI:
			for (int i = 0; i < list_dea_input_kpi.getItemCount(); i++) {
				list_dea_kpi.addItem(list_dea_input_kpi.getItemText(i));
			}
			list_dea_input_kpi.clear();
			for (int i = 0; i < list_dea_output_kpi.getItemCount(); i++) {
				list_dea_kpi.addItem(list_dea_output_kpi.getItemText(i));
			}
			list_dea_output_kpi.clear();
			break;
		case OUTPUT_KPI:
			for (int i = 0; i < list_dea_output_kpi.getItemCount(); i++) {
				list_dea_kpi.addItem(list_dea_output_kpi.getItemText(i));
			}
			list_dea_output_kpi.clear();
			break;
		default:
			break;
		}

		validateKPIList();
	}

	/**
	 * Gets the data miner response.
	 *
	 * @return the data miner response
	 */
	public DataMinerResponse getDataMinerResponse() {
		return dmResponse;
	}

	/**
	 * Gets the data input parameters.
	 *
	 * @return the data input parameters
	 */
	public Map<String, String> getDataInputParameters() {

		return dataInputParameters;
	}

	/**
	 * 
	 * @return DEA description
	 */
	public String getDeaDescription() {
		return deaDescription;
	}

	/**
	 * 
	 * 
	 * @param deaDescription
	 *            DEA description
	 */
	public void setDeaDescription(String deaDescription) {
		this.deaDescription = deaDescription;
	}

	/**
	 * Show alert.
	 *
	 * @param error
	 *            the error
	 * @param type
	 *            the type
	 * @param closable
	 *            the closable
	 * @param panel
	 *            the panel
	 */
	private void showAlert(String error, AlertType type, boolean closable, ComplexPanel panel) {
		Alert alert = new Alert(error);
		alert.setType(type);
		alert.setClose(closable);
		alert.getElement().getStyle().setMargin(10, Unit.PX);
		panel.add(alert);
	}

	/**
	 * Adds the selected kp is.
	 *
	 * @param selectedKPIs
	 *            the selected kp is
	 */
	public void addSelectedKPIs(List<KPI> selectedKPIs) {

		this.selectedKPIs = selectedKPIs;

	}

	/**
	 * Adds the selected areas.
	 *
	 * @param listAreas
	 *            the list areas
	 */
	/*
	 * public void addSelectedAreas(List<String> listAreas) {
	 * 
	 * this.selectedAreas = listAreas;
	 * 
	 * }
	 */

	/**
	 * Gets the KPI for name.
	 *
	 * @param name
	 *            the name
	 * @return the KPI for name
	 */
	public KPI getKPIForName(String name) {

		GWT.log("Searching KPI name: " + name);
		KPI foundKPI = null;
		String purgedName = name.trim();
		for (KPI kpi : selectedKPIs) {
			String purgedKPIName = kpi.getName().trim();
			if (purgedKPIName.compareToIgnoreCase(purgedName) == 0) {
				foundKPI = kpi;
				break;
			}
		}
		GWT.log("FOUND KPI: " + foundKPI);
		return foundKPI;
	}

	/**
	 * Adds the parameters.
	 *
	 * @param keyToGet
	 *            the key to get
	 * @param parameters
	 *            the parameters
	 * @param toShowBatchTypeValue
	 *            label to show batch type value
	 */
	public void addParameters(String keyToGet, Map<String, List<String>> parameters, String toShowBatchTypeValue) {

		final FlexTable flexTable = new FlexTable();
		flexTable.setStyleName("colgrouptable");

		try {
			List<String> dataInputs = parameters.get(keyToGet);

			if (dataInputs == null || dataInputs.isEmpty())
				return;

			dataInputParameters = new HashMap<String, String>();

			String theDataInputs = dataInputs.get(0);
			String[] splittedParams = theDataInputs.split(";");

			for (String splitParam : splittedParams) {
				try {
					String[] keyvalue = splitParam.split("=");
					dataInputParameters.put(keyvalue[0], keyvalue[1]);
				} catch (Exception e) {

				}
			}

			flexTable.setWidget(0, 0, new HTML("Batch Type:"));
			flexTable.setWidget(0, 1, new HTML(toShowBatchTypeValue));
			flexTable.setWidget(1, 0, new HTML("Level:"));
			flexTable.setWidget(1, 1, new HTML(dataInputParameters.get(PerformFishAnalyticsConstant.DM_SCALEP_PARAM)));
			// flexTable.setWidget(2, 0, new HTML("Batch ID:"));
			// flexTable.setWidget(2, 1, new
			// HTML(dataInputParameters.get(PerformFishAnalyticsConstant.DM_FOCUS_PARAM)));

			String KPINames = "";
			for (KPI kpi : selectedKPIs) {
				KPINames += kpi.getName() + ", ";
			}
			KPINames = KPINames.substring(0, KPINames.length() - 2);
			flexTable.setWidget(2, 0, new HTML("KPIs:"));
			flexTable.setWidget(2, 1, new HTML(KPINames));

			fillDeaListBoxes();

		} catch (Exception e) {
			GWT.log("Error in addParameters for DeaResulPanel: " + e.getLocalizedMessage(), e);
		}

		field_parameters_container.add(flexTable);
	}

	/**
	 * Fill dea list boxes.
	 */
	private void fillDeaListBoxes() {
		list_dea_kpi.clear();
		for (KPI kpi : selectedKPIs) {
			list_dea_kpi.addItem(kpi.getName());
		}
		list_dea_input_kpi.clear();
		list_dea_output_kpi.clear();

	}

	/**
	 * Gets the KPI for indexes.
	 *
	 * @param rowIndex
	 *            the row index
	 * @param columnIndex
	 *            the column index
	 * @return the KPI for indexes
	 */
	public List<KPI> getKPIForIndexes(int rowIndex, int columnIndex) {
		String key = generateKey(rowIndex, columnIndex);
		return kpiMapPointers.get(key);
	}

	/**
	 * Generate key.
	 *
	 * @param rowIndex
	 *            the row index
	 * @param columnIndex
	 *            the column index
	 * @return the string
	 */
	private String generateKey(int rowIndex, int columnIndex) {
		return rowIndex + "-" + columnIndex;
	}

	/**
	 * Call dea analysis.
	 *
	 * @param inputKPINames
	 *            the input kpi names
	 * @param outputKPINames
	 *            the output kpi names
	 * @param button
	 *            the button
	 */
	private void callDeaAnalysis(List<String> inputKPINames, List<String> outputKPINames, Button button) {

		HorizontalPanel hp = new HorizontalPanel();
		hp.getElement().addClassName("ext-horizontal-panel-without-margin");

		HTMLPanel resultContainer = new HTMLPanel(""); // automatically creates
														// a div.
		callNumber++;
		String deaAnalysisResultNumber = RESULTID + "_" + callNumber;
		resultContainer.getElement().setId(deaAnalysisResultNumber);
		VerticalPanel deaAnalysis = new VerticalPanel();
		resultContainer.add(deaAnalysis);
		hp.add(resultContainer);
		uib_vp_deanalanlysis_algorithm_container.add(hp);
		uib_vp_deanalanlysis_algorithm_container.getElement().setId("DeaAnalysisResults");

		// TODO
		final FlexTable flexTable = new FlexTable();
		flexTable.setStyleName("colgrouptable");
		flexTable.getElement().setId("DeaAnalysisResultParameters_" + callNumber);

		StringBuilder stringInputKPINamesBuilder = new StringBuilder();
		List<KPI> inputKPI = new ArrayList<KPI>();
		for (String kpiName : inputKPINames) {
			inputKPI.add(getKPIForName(kpiName));
			if (stringInputKPINamesBuilder.toString() == null || stringInputKPINamesBuilder.toString().isEmpty()) {
				stringInputKPINamesBuilder.append(kpiName);
			} else {
				stringInputKPINamesBuilder.append(",");
				stringInputKPINamesBuilder.append(kpiName);
			}

		}

		StringBuilder stringOutputKPINamesBuilder = new StringBuilder();
		List<KPI> outputKPI = new ArrayList<KPI>();
		for (String kpiName : outputKPINames) {
			outputKPI.add(getKPIForName(kpiName));
			if (stringOutputKPINamesBuilder.toString() == null || stringOutputKPINamesBuilder.toString().isEmpty()) {
				stringOutputKPINamesBuilder.append(kpiName);
			} else {
				stringOutputKPINamesBuilder.append(",");
				stringOutputKPINamesBuilder.append(kpiName);
			}
		}

		/////

		HTML selectedInputKPIs = new HTML(stringInputKPINamesBuilder.toString());
		selectedInputKPIs.getElement().setId("DeaAnalysisResultParameterInputKPIs_" + callNumber);

		HTML selectedOutputKPIs = new HTML(stringOutputKPINamesBuilder.toString());
		selectedOutputKPIs.getElement().setId("DeaAnalysisResultParameterOutputKPIs_" + callNumber);

		flexTable.setWidget(0, 0, new HTML("Selected Input KPIs:"));
		flexTable.setWidget(0, 1, selectedInputKPIs);
		flexTable.setWidget(1, 0, new HTML("Selected Output KPIs:"));
		flexTable.setWidget(1, 1, selectedOutputKPIs);
		flexTable.addStyleName("the_margin_top_bottom");

		deaAnalysis.add(flexTable);

		/////

		GWT.log("Calling Dea Analysys... with input: " + inputKPI + " and output: " + outputKPI);
		uib_vp_deanalanlysis_algorithm.setVisible(true);
		callDataMinerServiceForChart(dataInputParameters, inputKPI, outputKPI, DataMinerAlgorithms.DEA_ANALYSIS,
				deaAnalysis, callNumber);
	}

	/**
	 * Display output files as static entities.
	 *
	 * @param dmResponse
	 *            the dm response
	 * @param chartType
	 *            the chart type
	 * @param inputKPIs
	 *            the input kp is
	 * @param outputKPIs
	 *            the output kp is
	 * @param focusID
	 *            the focus id
	 * @param container
	 *            the container
	 * @param displayError
	 *            the display error
	 * @param deaAnalysisResultNumber
	 *            TODO
	 */
	private void displayOutputFilesAsStaticEntities(DataMinerResponse dmResponse, final DataMinerAlgorithms chartType,
			List<KPI> inputKPIs, List<KPI> outputKPIs, final String focusID, final Panel container,
			boolean displayError, int callNumber) {

		String title = displayError ? "No results " : "";

		if (displayError) {
			Alert alert = new Alert(title);
			alert.setType(AlertType.ERROR);
			alert.setClose(false);
			alert.getElement().getStyle().setMargin(10, Unit.PX);
			container.add(alert);
			return;
		}

		final FlexTable resultsPanel = new FlexTable();
		container.add(resultsPanel);

		final String toTitle = title;
		int outputNumber = 0;
		for (final OutputFile outputFile : dmResponse.getListOutput()) {
			outputNumber++;
			final String outputId = RESULTID + "_" + callNumber + "_" + outputNumber;
			switch (outputFile.getDataType()) {
			case IMAGE:
				PerformFishAnalyticsServiceAsync.Util.getInstance().getImageFile(outputFile,
						new AsyncCallback<String>() {

							@Override
							public void onFailure(Throwable caught) {
								showAlert(caught.getMessage(), AlertType.ERROR, true,
										uib_vp_deanalanlysis_request_container);

							}

							@Override
							public void onSuccess(String base64Content) {

								String title = toTitle;
								ShowResult showResult = new ShowResult(outputId, title, TextAlign.LEFT,
										outputFile.getDataType());
								showResult.showImage(base64Content);
								showOrderedResults(resultsPanel, outputFile.getName(), showResult);

							}
						});
				break;
			case CSV:
				PerformFishAnalyticsServiceAsync.Util.getInstance().getCSVFile(outputFile, true,
						new AsyncCallback<CSVFile>() {
							@Override
							public void onFailure(Throwable caught) {
								showAlert(caught.getMessage(), AlertType.ERROR, true,
										uib_vp_deanalanlysis_request_container);
							}

							@Override
							public void onSuccess(CSVFile result) {
								GWT.log("Displaying: " + result);
								String cssTableStyle = "simpletable";
								String title = toTitle;

								ShowResult showResult = new ShowResult(outputId, title, TextAlign.LEFT,
										outputFile.getDataType());
								showResult.showCSVFile(result, cssTableStyle);
								showOrderedResults(resultsPanel, outputFile.getName(), showResult);
							}
						});
				break;

			default:
				break;
			}

		}

		outputNumbers.put(RESULTID + "_" + callNumber, outputNumber);

	}

	private void showOrderedResults(FlexTable flexTable, String outputName, ShowResult showResult) {
		if (outputName == null || outputName.isEmpty()) {
			flexTable.setWidget(5, 0, showResult);
		} else {
			if (outputName.contains("dea")) {
				flexTable.setWidget(0, 0, showResult);
			} else {
				if (outputName.contains("efficiency_legend")) {
					flexTable.setWidget(1, 0, showResult);
				} else {
					if (outputName.contains("efficiency_aggregated")) {
						flexTable.setWidget(2, 0, showResult);
					} else {
						if (outputName.contains("efficiency_per_id")) {
							flexTable.setWidget(3, 0, showResult);
						} else {
							flexTable.setWidget(4, 0, showResult);

						}
					}
				}
			}
		}

	}

	/**
	 * Call data miner service for chart.
	 *
	 * @param dataInputParameters
	 *            the data input parameters
	 * @param inputKPI
	 *            the input kpi
	 * @param outputKPI
	 *            the output kpi
	 * @param chartType
	 *            the chart type
	 * @param panel
	 *            the panel
	 * @param callNumber
	 *            call number
	 * @param focusID
	 *            the focus id
	 */
	private void callDataMinerServiceForChart(Map<String, String> dataInputParameters, final List<KPI> inputKPI,
			final List<KPI> outputKPI, final DataMinerAlgorithms chartType, final ComplexPanel panel,
			final int callNumber) {

		GWT.log("Call DM wiht Selected KPI: " + inputKPI);
		StringBuilder dataInputsFormatter = new StringBuilder();
		String scalePValue = dataInputParameters.get(PerformFishAnalyticsConstant.DM_SCALEP_PARAM);
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_SCALEP_PARAM + "=" + scalePValue + ";");
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_BATCHTYPE_PARAM + "="
				+ dataInputParameters.get(PerformFishAnalyticsConstant.DM_BATCHTYPE_PARAM) + ";");
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_CHARTTYPE_PARAM + "=" + chartType + ";");
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_FARMFILE_PARAM + "="
				+ dataInputParameters.get(PerformFishAnalyticsConstant.DM_FARMFILE_PARAM) + ";");

		if (inputKPI != null && inputKPI.size() > 0) {
			String kpiCodes = "";
			for (KPI kpi : inputKPI) {
				kpiCodes += kpi.getCode() + "|";
			}
			// remove last |
			kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);

			GWT.log("Input KPICodes: " + kpiCodes);
			// ADDING KPIs code
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_INPUT_KPI_PARAM + "=" + kpiCodes + ";");

		}

		if (outputKPI != null && outputKPI.size() > 0) {
			String kpiCodes = "";
			for (KPI kpi : outputKPI) {
				kpiCodes += kpi.getCode() + "|";
			}
			// remove last |
			kpiCodes = kpiCodes.substring(0, kpiCodes.length() - 1);

			GWT.log("Output KPICodes: " + kpiCodes);
			// ADDING KPIs code
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_OUTPUT_KPI_PARAM + "=" + kpiCodes + ";");

		}

		final String focusID = dataInputParameters.get(PerformFishAnalyticsConstant.DM_FOCUS_PARAM);
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_FOCUS_PARAM + "=" + focusID + ";");

		String dataInParameters = dataInputsFormatter.toString();
		GWT.log("Calling DM service with client input parameters: " + dataInParameters);

		Map<String, List<String>> mapParameters = new HashMap<String, List<String>>();
		mapParameters.put(PerformFishAnalyticsConstant.DATA_INPUTS, Arrays.asList(dataInParameters));

		final LoaderIcon loaderIcon = new LoaderIcon(
				"Submitting request to " + DataMinerAlgorithms.DEA_ANALYSIS.getTitle() + " Analysis...");
		loaderIcon.getElement().getStyle().setMarginTop(10, Unit.PX);
		loaderIcon.setVisible(true);
		panel.setVisible(true);
		panel.add(loaderIcon);
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			@Override
			public void execute() {
				loaderIcon.setFocus(true);
			}
		});

		PerformFishAnalyticsServiceAsync.Util.getInstance().callingDataMinerPerformFishAnalysis(mapParameters,
				new AsyncCallback<DataMinerResponse>() {

					@Override
					public void onSuccess(DataMinerResponse dmResponse) {
						loaderIcon.setVisible(false);
						// field_unary_algorithm.setVisible(true);
						GWT.log("I'm displaying: " + dmResponse);
						displayOutputFilesAsStaticEntities(dmResponse, chartType, inputKPI, outputKPI, focusID, panel,
								false, callNumber);
					}

					@Override
					public void onFailure(Throwable caught) {
						loaderIcon.setVisible(false);
						displayOutputFilesAsStaticEntities(dmResponse, chartType, inputKPI, outputKPI, focusID, panel,
								true, callNumber);

					}
				});
	}

	private static native void saveFilePDF(DeaPanelResult chart, String filename)/*-{
		var that = this;
		console.log('saveFilePDF()');

		//deaAnalysisResult.rows[0]

		//var canvas = @org.gcube.portlets.user.performfishanalytics.client.viewbinder.DeaPanelResult::getCanvas(Lorg/gcube/portlets/user/accountingdashboard/client/application/mainarea/report/chartjs/Chart;)(chart);
		//console.log(Object.getOwnPropertyNames(deaAnalysisResult));
		//for ( property in deaAnalysisResults) {
		//	console.log(''+property+':'+deaAnalysisResults[property]);
		//}

		//console.log('Save file pdf');
		//var image = canvas.toDataURL("image/png", 1.0).replace("image/png",
		//		"image/octet-stream");
		//var doc = new jsPDF('landscape');
		//doc.addImage(image, 'JPEG', 10, 10, 280, 150);

		var pdoc = new jsPDF("p", "mm", "a4");
		pdoc.setProperties({
			title : 'DEA Analysis',
			subject : 'PerformFish DEA Analysis',
			author : 'PerformFish',
			keywords : 'PerformFish, DEA Analysis',
			creator : 'D4Science'
		});

		var lMargin = 15; //left margin in mm
		var rMargin = 15; //right margin in mm
		var tMargin = 15; //top margin in mm
		var bMargin = 15; //bottom margin in mm
		var pdfWidthInMM = 210; // width of A4 in mm
		var pdfHeightInMM = 297; // height of A4 in mm
		var pageCenter = pdfWidthInMM / 2;

		pdoc.setFontSize(24);
		var title = "DEA Analysis";
		var titleHeight = pdoc.getLineHeight(title) / pdoc.internal.scaleFactor
		var xPos = lMargin;
		var yPos = tMargin;
		pdoc.text(title, pageCenter, yPos, 'center');
		yPos += titleHeight;

		var deaDescription = chart.@org.gcube.portlets.user.performfishanalytics.client.viewbinder.DeaPanelResult::deaDescription;

		pdoc.setFontSize(10);
		var lineHeight = pdoc.getLineHeight(deaDescription)
				/ pdoc.internal.scaleFactor
		var splittedDeaDescription = pdoc.splitTextToSize(deaDescription,
				(pdfWidthInMM - lMargin - rMargin));
		var lines = splittedDeaDescription.length // splitted text is a string array
		var deaDescriptionHeight = lines * lineHeight

		pdoc.text(splittedDeaDescription, xPos, yPos, 'left');
		yPos += deaDescriptionHeight;

		//var table = [ {
		//	"kpisType" : " Input KPIs: ",
		//	"kpisValues" : deaAnalysisResultParametersInputKPIs
		//}, {
		//	"kpisType" : " Output KPIs: ",
		//	"kpisValues" : deaAnalysisResultParametersOutputKPIs
		//} ];
		//doc.table(10, 10, table, null, {
		//	left : lMargin,
		//	top : 30,
		//	bottom : 10,
		//	width : pdfInMM,
		//	autoSize : false,
		//	printHeaders : true
		//});

		// It can parse html:
		//doc.autoTable({
		//	html : '#my-table'
		//});

		//startY: doc.autoTableEndPosY() + 50
		//doc.autoTable(res.columns, res.data, {
		//	startY : doc.autoTableEndPosY() + 50,
		//	afterPageContent : function(data) {
		//		doc.setFontSize(20)
		//		doc.text("After page content", 50, height
		//				- data.settings.margin.bottom - 20);
		//	}
		//});

		//var matrix = [];
		//for (var i = 0; i < 1000; i++) {
		//	matrix[i] = [];
		//	for (var j = 0; j < 9; j++) {
		//		matrix[i][j] = "Hello";
		//	}
		//}

		yPos += 2;
		pdoc.text("Analysis:", xPos, yPos, 'left');
		yPos += 6;

		var callNumber = @org.gcube.portlets.user.performfishanalytics.client.viewbinder.DeaPanelResult::callNumber;
		console.log('CallNumber: ' + callNumber);

		for (var n = 1; n <= callNumber; n++) {
			var deaAnalysisResultParametersInputKPIs = $doc
					.getElementById('DeaAnalysisResultParameterInputKPIs_' + n).innerHTML;

			console.log('InputKPIs: ' + deaAnalysisResultParametersInputKPIs);
			var deaAnalysisResultParametersOutputKPIs = $doc
					.getElementById('DeaAnalysisResultParameterOutputKPIs_' + n).innerHTML;
			console.log('OuputKPIs: ' + deaAnalysisResultParametersOutputKPIs);

			pdoc.autoTable({
				theme : 'plain',
				startY : yPos,
				pageBreak : 'auto',
				columnStyles : {
					0 : {
						halign : 'left',
						fillColor : [ 211, 225, 241 ]
					}
				},
				head : null,
				body : [
						[ 'Selected Input KPIs',
								deaAnalysisResultParametersInputKPIs ],
						[ 'Selected Output KPIs',
								deaAnalysisResultParametersOutputKPIs ] ]
			});

			yPos = pdoc.previousAutoTable.finalY + 6;

			var deaAnalysisResults = $doc.getElementById('DeaAnalysisResults');

			var deaAnalysisResult = $doc.getElementById('DeaAnalysisResult_'
					+ n);

			console.log("DeaAnalysisResult: " + n);

			var outputNumber = @org.gcube.portlets.user.performfishanalytics.client.viewbinder.DeaPanelResult::getOutputNumber(Ljava/lang/String;)('DeaAnalysisResult_'+ n);
			console.log('OutputNumber: ' + outputNumber);

			for (var i = 1; i <= outputNumber; i++) {

				var deaOutputId = 'DeaAnalysisResult_' + n + '_' + i;
				console.log('DeaOutputId: ' + deaOutputId);

				var deaOutputNumber = $doc.getElementById(deaOutputId);

				console.log('DeaOutputNumber: ' + deaOutputNumber.innerHTML);
				var resultType = deaOutputNumber.className;

				console.log('DeaOutputNumber className: ' + resultType);

				if (typeof resultType !== 'undefined' && resultType !== null
						&& resultType !== '') {

					if (resultType == 'csv') {
						console.log('Result Type csv: ' + i);
						// foo could get resolved and it's defined

						var childrenTable = deaOutputNumber.children;
						var secondDiv = childrenTable[0].rows[1].cells[0];
						var secondTable = secondDiv.children[0].children[0];

						console.log('CSV SecondTable: ' + secondTable);

						pdoc.autoTable({
							theme : 'grid',
							startY : yPos,
							pageBreak : 'auto',
							bodyStyles : {
								fontSize : 9
							},
							html : secondTable
						});

						yPos = pdoc.previousAutoTable.finalY + 6;
					} else {
						if (resultType == 'image') {
							console.log('Result Type image: ' + i);
							var childrenTable = deaOutputNumber.children;
							var secondDiv = childrenTable[0].rows[1].cells[0];
							var imageElement = secondDiv.getElementsByTagName(
									'img').item(0);
							console.log('Image element: ' + imageElement);
							pdoc.addImage(imageElement, lMargin, yPos);
							console.log('Image Height: ' + imageElement.height);
							yPos += (imageElement.height * 0.26458333);

						} else {
							console.log('Result Type unknow: ' + i);
						}

					}
				} else {
					console.log('Result Type is undefined:' + i);
				}

			}
		}
		console.log('Done');

		pdoc.save(filename);

	}-*/;

	// var HtmlWidth = deaAnalysisResult.offsetWidth;
	// var HtmlHeight = deaAnalysisResult.offsetHeight;
	// var canvas_image_width = HtmlWidth;
	// var canvas_image_height = HtmlHeight;
	//
	// var totalPDFPages = Math.ceil(HtmlHeight / pdfHeightInMM) - 1;
	//
	//
	//
	// html2canvas(deaOutputNumber, {
	// allowTaint : true,
	// }).then(
	// function(canvas) {
	// canvas.getContext('2d');
	//
	// console.log("Canvas: " + canvas.width + " x "
	// + canvas.height);
	// //1187 x 2388
	// var width = pdoc.internal.pageSize.getWidth();
	// var height = pdoc.internal.pageSize.getHeight();
	// console.log("Page: " + width + " x " + height);
	// var canvasWInMM = canvas.width * 0.26458333333;
	// var canvasHInMM = canvas.height * 0.26458333333;
	// totalPDFPages = Math.ceil(canvasHInMM / height) - 1;
	//
	// var imgData = canvas.toDataURL("image/jpeg", 1);
	// pdoc.addPage('a4','p');
	//
	// //Add to first page
	// pdoc.addImage(imgData, 'JPG', 10, -yPos+10, width-20,
	// height-20);
	//
	//// for (var i = 1; i <= totalPDFPages; i++) {
	//// pdoc.addPage('a4', 'p');
	//// }
	//// pdoc.addImage(imgData, 'JPG', 5, -height - 60, width
	//// - rMargin, height);
	////
	// pdoc.save(filename);
	//
	// });

	// var pdoc = new jsPDF();
	// pdoc.setProperties({
	// title : 'DEA Analysis',
	// subject : 'PerformFish DEA Analysis',
	// author : 'PerformFish',
	// keywords : 'PerformFish, DEA Analysis',
	// creator : 'D4Science'
	// });
	//
	// pdoc.html($doc.body, {
	// html2canvas : {
	// quality: 4,
	// scale: 0.2
	// },
	// callback : function(doc) {
	// doc.save();
	// }
	// });

	// pdoc.html(deaAnalysisResults, {
	// callback : function(doc) {
	// doc.save();
	// }
	// });

	// <script>
	//
	// var doc = new jsPDF();
	// var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABy...
	// doc.setFontSize(40);
	// doc.text(30, 20, 'Hello world!');
	// doc.addImage(imgData, 'PNG', 15, 40, 200, 114);
	// doc.output('datauri');
	// </script>
}
