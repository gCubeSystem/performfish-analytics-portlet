/**
 *
 */
package org.gcube.portlets.user.performfishanalytics.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.TextResource;


/**
 * The Interface PerformFishResources.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * May 8, 2019
 */
public interface PerformFishResources extends ClientBundle {

	public static final PerformFishResources INSTANCE = GWT.create(PerformFishResources.class);

	/**
	 * Loading.
	 *
	 * @return the image resource
	 */
	@Source("loading4.gif")
	ImageResource loading();

	/**
	 * Top.
	 *
	 * @return the image resource
	 */
	@Source("top.png")
	ImageResource top();

	/**
	 * Maven.
	 *
	 * @return the image resource
	 */
	@Source("maven.png")
	ImageResource maven();

	/**
	 * Wiki.
	 *
	 * @return the image resource
	 */
	@Source("wiki.png")
	ImageResource wiki();

	/**
	 * Javadoc.
	 *
	 * @return the image resource
	 */
	@Source("javadoc.png")
	ImageResource javadoc();

	/**
	 * Download.
	 *
	 * @return the image resource
	 */
	@Source("download.png")
	ImageResource download();

	/**
	 * Github.
	 *
	 * @return the image resource
	 */
	@Source("github.png")
	ImageResource github();
	
	@Source("Error_Page.html")
	public TextResource errorPage();
	
	@Source("Batch_BOXPLOT")
	public TextResource batch_BOXPLOT();
	
	@Source("Farm_BOXPLOT")
	public TextResource farm_BOXPLOT();
	
	@Source("Batch_SCATTER")
	public TextResource batch_SCATTER();
	
	@Source("Farm_SCATTER")
	public TextResource farm_SCATTER();
	
	@Source("Batch_SPEEDOMETER")
	public TextResource batch_SPEEDOMETER();
	
	@Source("Farm_SPEEDOMETER")
	public TextResource farm_SPEEDOMETER();
	
	@Source("Batch_CORRELATION")
	public TextResource batch_CORRELATION();
	
	@Source("Farm_CORRELATION")
	public TextResource farm_CORRELATION();
	
	
	@Source("Batch_DEAANALYSIS")
	public TextResource batch_DEAANALYSIS();
	
	@Source("Farm_DEAANALYSIS")
	public TextResource farm_DEAANALYSIS();
	
	@Source("BatchFarm_SYNOPTICTABLE")
	public TextResource synopticTable();
	
	@Source("es6-promise.auto.min.js")
	TextResource es6PromiseAuto();
	
	@Source("jspdf.min.js")
	TextResource jsPDF();
	
	@Source("jspdf.plugin.autotable.js")
	TextResource autotable();
	
	@Source("canvas.js")
	TextResource canvas();
	
	@Source("html2canvas.min.js")
	TextResource html2canvas();
	
	@Source("cell.js")
	TextResource cell();
	
	//@Source("html2pdf.min.js")
	//TextResource html2pdf();

}
