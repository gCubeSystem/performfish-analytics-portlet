/**
 *
 */
package org.gcube.portlets.user.performfishanalytics.client.viewbinder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.user.performfishanalytics.client.DataMinerAlgorithms;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsServiceAsync;
import org.gcube.portlets.user.performfishanalytics.client.controllers.PerformFishAnalyticsController;
import org.gcube.portlets.user.performfishanalytics.client.event.CallAlgorithmEvent;
import org.gcube.portlets.user.performfishanalytics.client.view.util.CorrelationValueToColourUtil;
import org.gcube.portlets.user.performfishanalytics.shared.FileContentType;
import org.gcube.portlets.user.performfishanalytics.shared.KPI;
import org.gcube.portlets.user.performfishanalytics.shared.OutputFile;
import org.gcube.portlets.user.performfishanalytics.shared.csv.CSVFile;
import org.gcube.portlets.user.performfishanalytics.shared.csv.CSVRow;
import org.gcube.portlets.user.performfishanalytics.shared.dataminer.DataMinerResponse;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class AnalyticsPanelResult.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jan 29, 2019
 */
public class CorrelationPanelResult extends Composite {

	/**
	 *
	 */

	private static CorrelationPanelResultUiBinder uiBinder = GWT.create(CorrelationPanelResultUiBinder.class);

	/**
	 * The Interface AnalyticsPanelResultUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR
	 *         (francesco.mangiacrapa@isti.cnr.it) Jan 30, 2019
	 */
	interface CorrelationPanelResultUiBinder extends UiBinder<Widget, CorrelationPanelResult> {
	}

	@UiField
	HTMLPanel field_html_panel;

	@UiField
	VerticalPanel field_parameters_container;

	@UiField
	VerticalPanel uib_vp_correlation_results_container;

	@UiField
	VerticalPanel field_unary_algorithm_container;

	@UiField
	VerticalPanel field_binary_algorithm;

	@UiField
	VerticalPanel field_binary_algorithm_container;

	@UiField
	VerticalPanel field_unary_algorithm;

	@UiField
	Label uib_label_focus_id;

	@UiField
	ControlGroup cg_focus_id_correlation;

	@UiField
	ListBox field_list_focus_id_correlation;

	@UiField
	Button uib_save_pdf_1;

	private Map<String, String> dataInputParameters;

	private DataMinerResponse dmResponse;

	private Map<String, List<KPI>> kpiMapPointers = new HashMap<String, List<KPI>>();

	private List<KPI> selectedKPIs;

	private List<String> selectedAreas;

	private Map<String, CSVFile> csvGenerated = new HashMap<String, CSVFile>();

	private CorrelationPanelResult analyticsPanelResult;

	private int outputNumber;
	private int requestId = 0;

	/**
	 * 
	 * @param reqId
	 *            Request Identifier
	 * @param tabTitle
	 *            Title
	 * @param tabDescription
	 *            Description
	 */
	public CorrelationPanelResult(int reqId, final String tabTitle, final String tabDescription) {
		GWT.log("RequestID: " + reqId);
		requestId = reqId;

		initWidget(uiBinder.createAndBindUi(this));

		analyticsPanelResult = this;

		uib_save_pdf_1.setIcon(IconType.PRINT);
		uib_save_pdf_1.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				saveFilePDF(analyticsPanelResult, tabTitle, tabTitle, tabDescription, requestId, outputNumber);
			}
		});
	}

	/**
	 * Adds the selected areas.
	 *
	 * @param listAreas
	 *            the list areas
	 */
	public void addSelectedAreas(List<String> listAreas) {

		this.selectedAreas = listAreas;

	}

	/**
	 * Gets the data miner response.
	 *
	 * @return the data miner response
	 */
	public DataMinerResponse getDataMinerResponse() {
		return dmResponse;
	}

	/**
	 * Gets the data input parameters.
	 *
	 * @return the data input parameters
	 */
	public Map<String, String> getDataInputParameters() {

		return dataInputParameters;
	}

	/**
	 * Show alert.
	 *
	 * @param error
	 *            the error
	 * @param type
	 *            the type
	 * @param closable
	 *            the closable
	 * @param panel
	 *            the panel
	 */
	private void showAlert(String error, AlertType type, boolean closable, ComplexPanel panel) {
		Alert alert = new Alert(error);
		alert.setType(type);
		alert.setClose(closable);
		alert.getElement().getStyle().setMargin(10, Unit.PX);
		panel.add(alert);
	}

	/**
	 * Adds the selected kp is.
	 *
	 * @param selectedKPIs
	 *            the selected kp is
	 */
	public void addSelectedKPIs(List<KPI> selectedKPIs) {

		this.selectedKPIs = selectedKPIs;

	}

	/**
	 * Gets the KPI for name.
	 *
	 * @param name
	 *            the name
	 * @return the KPI for name
	 */
	public KPI getKPIForName(String name) {

		GWT.log("Searching KPI name: " + name);
		KPI foundKPI = null;
		String purgedName = name.trim();
		for (KPI kpi : selectedKPIs) {
			String purgedKPIName = kpi.getName().trim();
			if (purgedKPIName.compareToIgnoreCase(purgedName) == 0) {
				foundKPI = kpi;
				break;
			}
		}
		GWT.log("FOUND KPI: " + foundKPI);
		return foundKPI;
	}

	/**
	 * Adds the parameters.
	 *
	 * @param keyToGet
	 *            the key to get
	 * @param parameters
	 *            the parameters
	 * @param toShowBatchTypeValue
	 *            label to show batch type value
	 */
	public void addParameters(String keyToGet, Map<String, List<String>> parameters, String toShowBatchTypeValue) {

		final FlexTable flexTable = new FlexTable();
		flexTable.getElement().setId("CorrelationParameterInputKPIs_"+requestId);
		flexTable.setStyleName("colgrouptable");

		try {
			List<String> dataInputs = parameters.get(keyToGet);

			if (dataInputs == null || dataInputs.isEmpty())
				return;

			dataInputParameters = new HashMap<String, String>();

			String theDataInputs = dataInputs.get(0);
			String[] splittedParams = theDataInputs.split(";");

			for (String splitParam : splittedParams) {
				try {
					String[] keyvalue = splitParam.split("=");
					dataInputParameters.put(keyvalue[0], keyvalue[1]);
				} catch (Exception e) {

				}
			}

			flexTable.setWidget(0, 0, new HTML("Batch Type:"));
			flexTable.setWidget(0, 1, new HTML(toShowBatchTypeValue));
			flexTable.setWidget(1, 0, new HTML("Level:"));
			flexTable.setWidget(1, 1, new HTML(dataInputParameters.get(PerformFishAnalyticsConstant.DM_SCALEP_PARAM)));

			String KPINames = "";
			for (KPI kpi : selectedKPIs) {
				KPINames += kpi.getName() + ", ";
			}
			KPINames = KPINames.substring(0, KPINames.length() - 2);
			flexTable.setWidget(2, 0, new HTML("KPIs:"));
			flexTable.setWidget(2, 1, new HTML(KPINames));

		} catch (Exception e) {
			// silent
		}

		field_parameters_container.add(flexTable);
	}

	/**
	 * Adds the list focus ids.
	 *
	 * @param listFocusIDs
	 *            the list focus I ds
	 */
	public void addListFocusIds(List<String> listFocusIDs) {
		for (String batchID : listFocusIDs) {
			field_list_focus_id_correlation.addItem(batchID, batchID);
		}
	}

	private String getOutputId() {
		outputNumber++;
		String outputId = "OutputId_" + requestId + "_" + outputNumber;
		GWT.log("Generated OutputId: " + outputId);
		return outputId;
	}


	/**
	 * Adds the results.
	 *
	 * @param dmResponse
	 *            the dm response
	 */
	public void addResults(DataMinerResponse dmResponse) {
		this.dmResponse = dmResponse;

		outputNumber = 0;
		for (OutputFile outputFile : dmResponse.getListOutput()) {

			if (outputFile.getDataType().equals(FileContentType.CSV)) {
				PerformFishAnalyticsServiceAsync.Util.getInstance().getCSVFile(outputFile, true,
						new AsyncCallback<CSVFile>() {

							@Override
							public void onFailure(Throwable caught) {
								showAlert(caught.getMessage(), AlertType.ERROR, true,
										uib_vp_correlation_results_container);

							}

							@Override
							public void onSuccess(CSVFile result) {

								csvGenerated.put(result.getFileName(), result);
								fillCorrelationMatrix();
							}
						});
			}

		}
	}

	private void fillCorrelationMatrix() {

		if (csvGenerated.size() < 2)
			return;

		String corrIndexFilename = null;
		String corrFilename = null;
		for (String fileName : csvGenerated.keySet()) {
			if (fileName.contains("index")) {
				corrIndexFilename = fileName;
			} else {
				corrFilename = fileName;
			}
		}

		GWT.log("Correlation Matrix Index File: " + corrIndexFilename);
		GWT.log("Correlation Matrix File: " + corrFilename);
		CSVFile corrIndexCsvFile = csvGenerated.get(corrIndexFilename);
		CSVFile corrCsvFile = csvGenerated.get(corrFilename);
		GWT.log("Correlation Matrix Index CSV: " + corrIndexCsvFile);
		GWT.log("Correlation Matrix CSV: " + corrCsvFile);

		HorizontalPanel hp = new HorizontalPanel();
		final FlexTable flexTable = new FlexTable();
		flexTable.getElement().setId(getOutputId());
		flexTable.setStyleName("fixedtable");
		flexTable.getElement().getStyle().setMarginBottom(10, Unit.PX);

		flexTable.setWidget(0, 0, new Label(""));

		CSVRow headerRow = corrIndexCsvFile.getHeaderRow();
		// HEADER
		for (int i = 1; i < headerRow.getListValues().size(); i++) {

			final String headerValue = headerRow.getListValues().get(i);
			HTML label = new HTML(headerValue);
			label.getElement().getStyle().setFontSize(16, Unit.PX);

			final int columnIndex = i;
			KPI kpi = getKPIForName(headerValue);
			fillKPIReferenceForIndex(0, columnIndex, Arrays.asList(kpi));
			flexTable.setWidget(0, i, label);
		}
		// DATA
		for (int i = 0; i < corrIndexCsvFile.getValueRows().size(); i++) {
			CSVRow row = corrIndexCsvFile.getValueRows().get(i);
			CSVRow rowMatrixColor = corrCsvFile.getValueRows().get(i);
			final int rowIndex = i + 1; // adding +1 for header row
			for (int j = 0; j < row.getListValues().size(); j++) {
				final String rowValue = row.getListValues().get(j);
				final String rowMatrixColorValue = rowMatrixColor.getListValues().get(j);
				final String theColor = CorrelationValueToColourUtil.getRGBColor(rowMatrixColorValue);
				final int columnIndex = j;
				final HTML buttonHTML = new HTML(rowValue);
				buttonHTML.addStyleName("my-active-html");
				if (j == 0) {
					HTML label = new HTML(rowValue);
					label.getElement().getStyle().setFontSize(16, Unit.PX);
					flexTable.setWidget(rowIndex, j, label);
					// rowValue is a KPI name
					KPI kpi = getKPIForName(rowValue);
					fillKPIReferenceForIndex(rowIndex, columnIndex, Arrays.asList(kpi));
					continue;
				}
				// diagonal
				else if (rowIndex == j) {
					HTML dg = new HTML(rowValue);
					dg.getElement().getStyle().setFontSize(18, Unit.PX);
					flexTable.setWidget(rowIndex, j, dg);
					continue;
					// j > 0
				} else {
					KPI columnKPI = getKPIForName(headerRow.getListValues().get(columnIndex));
					// Here the first index is the KPI name
					KPI rowKPI = getKPIForName(row.getListValues().get(0));
					fillKPIReferenceForIndex(rowIndex, columnIndex, Arrays.asList(columnKPI, rowKPI));

					buttonHTML.addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {

							// callScatterAndDeaChart(rowIndex, columnIndex,
							// button);
							callScatter(rowIndex, columnIndex, buttonHTML);
						}
					});
				}

				// button.setType(ButtonType.LINK);
				// APPLYING STYLE ONLY ON INTERNAL VALUES OF CSV
				if (j > 0)
					buttonHTML.getElement().getStyle().setColor("#000");

				buttonHTML.addAttachHandler(new AttachEvent.Handler() {

					@Override
					public void onAttachOrDetach(AttachEvent event) {
						if (theColor.startsWith("#")) {
							GWT.log("Setting backgrounf color: " + theColor);
							buttonHTML.getElement().getParentElement().getStyle().setBackgroundColor(theColor);
						}
					}
				});
				
				buttonHTML.getElement().getStyle().setBackgroundColor(theColor);
				flexTable.setWidget(rowIndex, j, buttonHTML);
			}

		}

		hp.add(flexTable);

		final FlexTable flexLegend = new FlexTable();
		flexLegend.getElement().setId(getOutputId());
		flexLegend.setStyleName("simpletable");
		Map<String, String> map = CorrelationValueToColourUtil.getMap();
		flexLegend.setWidget(0, 0, new Label("Legend"));
		flexLegend.setWidget(0, 1, new HTML(""));
		int i = 1;
		for (String key : map.keySet()) {
			final String rgbColor = map.get(key);
			final HTML theLegendColor = new HTML("");
			theLegendColor.getElement().getStyle().setBackgroundColor(rgbColor);
			flexLegend.setWidget(i, 0, theLegendColor);
			flexLegend.setWidget(i, 1, new HTML(key));
			theLegendColor.addAttachHandler(new AttachEvent.Handler() {

				@Override
				public void onAttachOrDetach(AttachEvent event) {
					theLegendColor.getElement().getParentElement().getStyle().setBackgroundColor(rgbColor);
				}
			});

			i++;
		}

		flexLegend.getElement().getStyle().setMarginLeft(15, Unit.PX);
		hp.add(flexLegend);
		uib_vp_correlation_results_container.insert(hp, 0);

	}

	/**
	 * Fill kpi reference for index.
	 *
	 * @param rowIndex
	 *            the row index
	 * @param columnIndex
	 *            the column index
	 * @param listKPI
	 *            the list kpi
	 */
	private void fillKPIReferenceForIndex(int rowIndex, int columnIndex, List<KPI> listKPI) {

		String key = generateKey(rowIndex, columnIndex);
		kpiMapPointers.put(key, listKPI);
	}

	/**
	 * Gets the KPI for indexes.
	 *
	 * @param rowIndex
	 *            the row index
	 * @param columnIndex
	 *            the column index
	 * @return the KPI for indexes
	 */
	public List<KPI> getKPIForIndexes(int rowIndex, int columnIndex) {
		String key = generateKey(rowIndex, columnIndex);
		return kpiMapPointers.get(key);
	}

	/**
	 * Generate key.
	 *
	 * @param rowIndex
	 *            the row index
	 * @param columnIndex
	 *            the column index
	 * @return the string
	 */
	private String generateKey(int rowIndex, int columnIndex) {
		return rowIndex + "-" + columnIndex;
	}

	/**
	 * Check valid focus id.
	 *
	 * @return the FocusID if it is valid, null otherwise.
	 */
	public String checkValidFocusID() {

		cg_focus_id_correlation.setType(ControlGroupType.NONE);

		// CHECK THE FOCUS ID VALUE
		String focusID = field_list_focus_id_correlation.getSelectedItemText();

		if (focusID == null || focusID.isEmpty()) {

			String msgError = "Could not execute a valid Analysis.";

			if (selectedAreas == null || selectedAreas.isEmpty()) {
				msgError += " Please select another parameters computation";
			} else {
				msgError += " Select at least the Area of your FARM";
			}

			showAlert(msgError, AlertType.ERROR, true, uib_vp_correlation_results_container);
			cg_focus_id_correlation.setType(ControlGroupType.ERROR);
			return null;
		}

		return focusID;
	}

	/**
	 * Call scatter.
	 *
	 * @param rowIndex
	 *            the row index
	 * @param columnIndex
	 *            the column index
	 * @param button
	 *            the button
	 */
	private void callScatter(int rowIndex, int columnIndex, HTML button) {

		String focusID = checkValidFocusID();

		if (focusID == null)
			return;

		GWT.log("Called ScatterChart at rowIndex: " + rowIndex + ", columnIndex: " + columnIndex);
		List<KPI> selectedKPI = getKPIForIndexes(rowIndex, columnIndex);
		GWT.log("Selected KPI: " + selectedKPI);
		PerformFishAnalyticsController.eventBus
				.fireEvent(new CallAlgorithmEvent(DataMinerAlgorithms.SCATTER, focusID, selectedKPI, null));
	}

	private static native void saveFilePDF(CorrelationPanelResult chart, String filename, String tabTitle,
			String tabDescr, int requestId, int outputNumber)/*-{
		var that = this;
		console.log('saveFilePDF()');

		var tTitle = tabTitle;
		var tDescr = tabDescr;

		var pdoc = new jsPDF("p", "mm", "a4");
		pdoc.setProperties({
			title : 'PerformFish ' + tTitle,
			subject : ' Results',
			author : 'PerformFish',
			keywords : 'PerformFish',
			creator : 'D4Science'
		});

		var lMargin = 15; //left margin in mm
		var rMargin = 15; //right margin in mm
		var tMargin = 15; //top margin in mm
		var bMargin = 15; //bottom margin in mm
		var pdfWidthInMM = 210; // width of A4 in mm
		var pdfHeightInMM = 297; // height of A4 in mm
		var pageCenter = pdfWidthInMM / 2;

		pdoc.setFontSize(24);
		var title = "PerformFish " + tTitle;
		var titleHeight = pdoc.getLineHeight(title) / pdoc.internal.scaleFactor
		var xPos = lMargin;
		var yPos = tMargin;
		pdoc.text(title, pageCenter, yPos, 'center');
		yPos += titleHeight;

		pdoc.setFontSize(10);
		var lineHeight = pdoc.getLineHeight(tDescr) / pdoc.internal.scaleFactor
		var splittedAnalysisDescription = pdoc.splitTextToSize(tDescr,
				(pdfWidthInMM - lMargin - rMargin));
		var lines = splittedAnalysisDescription.length // splitted text is a string array
		var analysisDescriptionHeight = lines * lineHeight

		pdoc.text(splittedAnalysisDescription, xPos, yPos, 'left');
		yPos += analysisDescriptionHeight;

		yPos += 4;
		pdoc.setFontSize(10);
		pdoc.setFontType("bold");
		pdoc.text("Parameters:", xPos, yPos, 'left');
		pdoc.setFontSize(10);
		pdoc.setFontType("normal");
		yPos += 4;

		var correlationParametersInputKPIs = $doc
				.getElementById('CorrelationParameterInputKPIs_'+requestId);

		console.log('InputKPIs: ' + correlationParametersInputKPIs);

		pdoc.autoTable({
			theme : 'plain',
			startY : yPos,
			pageBreak : 'auto',
			columnStyles : {
				0 : {
					halign : 'left',
					fillColor : [ 211, 225, 241 ]
				}
			},
			bodyStyles : {
				minCellWidth: 20
			},
			html : correlationParametersInputKPIs

		});

		//head : null,
		//	body : [
		//			[ 'Parameters',
		//					correlationParametersInputKPIs ]
		//		   ]

		yPos = pdoc.previousAutoTable.finalY + 6;

		yPos += 2;
		pdoc.setFontSize(10);
		pdoc.setFontType("bold");
		pdoc.text("CORRELATION Matrix:", xPos, yPos, 'left');
		pdoc.setFontSize(10);
		pdoc.setFontType("normal");
		yPos += 4;

		console.log('OutputNumber: ' + outputNumber);

		for (var i = 1; i <= outputNumber; i++) {

			var outputId = 'OutputId_' + requestId + '_' + i;
			console.log('OutputId: ' + outputId);

			var resultElement = $doc.getElementById(outputId);

			console.log('Result Element: ' + resultElement);
			var resultType = resultElement.className;

			console.log('Result Element className: ' + resultType);

			if (typeof resultType !== 'undefined' && resultType !== null
					&& resultType !== '') {

				if (resultType == 'fixedtable' || resultType == 'simpletable') {
					console.log('Result Type csv: ' + i);
					console.log('Result Content: ' + resultElement.innerHTML);

					var checkColor = function(data) {
						if (data.section === 'body') {
							console.log(data);
							if (typeof data.cell !== 'undefined'
									&& data.cell !== null) {
								if (typeof data.cell.raw !== 'undefined'
										&& data.cell.raw !== null) {
									console.log(data.cell.raw.innerHTML);
									var positive = 'rgb(255, 68, 45)';
									var negative = 'rgb(173, 216, 230)';
									var nonsignificant = 'rgb(230, 223, 0)';

									if (data.cell.raw.innerHTML
											.indexOf(positive) >= 0) {
										data.cell.styles.fillColor = [ 255, 68,
												45 ];
									} else {
										if (data.cell.raw.innerHTML
												.indexOf(negative) >= 0) {
											data.cell.styles.fillColor = [ 173,
													216, 230 ];
										} else {
											if (data.cell.raw.innerHTML
													.indexOf(nonsignificant) >= 0) {
												data.cell.styles.fillColor = [
														230, 223, 0 ];
											} else {

											}

										}

									}
								}
							}

						}
					};

					pdoc.autoTable({
						theme : 'grid',
						startY : yPos,
						pageBreak : 'auto',
						bodyStyles : {
							fontSize : 9
						},
						didParseCell : checkColor,
						html : resultElement
					});

					yPos = pdoc.previousAutoTable.finalY + 6;
				} else {
					if (resultType == 'image') {
						console.log('Result Type image: ' + i);
						var childrenTable = outputNumber.children;
						var secondDiv = childrenTable[0].rows[1].cells[0];
						var imageElement = secondDiv
								.getElementsByTagName('img').item(0);
						console.log('Image element: ' + imageElement);
						pdoc.addImage(imageElement, lMargin, yPos);
						console.log('Image Height: ' + imageElement.height);
						yPos += (imageElement.height * 0.26458333);

					} else {
						console.log('Result Type unknow: ' + i);
					}

				}
			} else {
				console.log('Result Type is undefined:' + i);
			}

			if (i == outputNumber) {

			} else {
				if (pdfHeightInMM - yPos < pdfHeightInMM / 7) {
					pdoc.addPage();
					yPos = tMargin; // Restart position
				}
			}

		}

			var focusIDElement = $doc
				.getElementById('field_list_focus_id_correlation');
		console.log('FocusID Element: ' + focusIDElement);
		var selectedIndex = focusIDElement.selectedIndex;
		if (selectedIndex > -1) {
			var value = focusIDElement[selectedIndex].innerText;
			yPos += 2;
			pdoc.setFontSize(10);
			pdoc.setFontType("normal");
			pdoc.text("Focus ID: "+value, xPos, yPos, 'left');
			pdoc.setFontSize(10);
			pdoc.setFontType("normal");
			yPos += 4;

		}

		var numberOfPages = pdoc.internal.getNumberOfPages()
		console.log('NumberOfPages: ' + numberOfPages);
		for (var k = 1; k <= numberOfPages; k++) {
			pdoc.setPage(k);
			console.log('CurrentPage: ' + k);
			var footner = 'Page ' + k + "/" + numberOfPages;
			pdoc.text(footner, pageCenter, pdfHeightInMM - 7, 'center');

		}

		console.log('Done');

		pdoc.save(filename);

	}-*/;


}
