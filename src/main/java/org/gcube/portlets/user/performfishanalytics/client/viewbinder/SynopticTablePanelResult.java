package org.gcube.portlets.user.performfishanalytics.client.viewbinder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.user.performfishanalytics.client.DataMinerAlgorithms;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsConstant;
import org.gcube.portlets.user.performfishanalytics.client.PerformFishAnalyticsServiceAsync;
import org.gcube.portlets.user.performfishanalytics.client.controllers.PerformFishAnalyticsViewController;
import org.gcube.portlets.user.performfishanalytics.client.resources.PerformFishResources;
import org.gcube.portlets.user.performfishanalytics.client.view.LoaderIcon;
import org.gcube.portlets.user.performfishanalytics.shared.FileContentType;
import org.gcube.portlets.user.performfishanalytics.shared.KPI;
import org.gcube.portlets.user.performfishanalytics.shared.OutputFile;
import org.gcube.portlets.user.performfishanalytics.shared.csv.CSVFile;
import org.gcube.portlets.user.performfishanalytics.shared.dataminer.DataMinerResponse;
import org.gcube.portlets.user.performfishanalytics.shared.performfishservice.PerformFishResponse;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Panel;

public class SynopticTablePanelResult {

	private Map<Integer, FlexTable> synopticOrderBy = new HashMap<Integer, FlexTable>();

	private PerformFishAnalyticsViewController viewController;
	private int requestId;
	private int outputIteration = 1;
	private int outputNumber = 0;

	private Button savePDFButton;

	public SynopticTablePanelResult(int reqId, PerformFishAnalyticsViewController viewController,
			PerformFishResponse performFishResponse, final Map<String, List<String>> performFishRequestParameters) {
		GWT.log("RequestID: " + reqId);
		requestId = reqId;
		this.viewController = viewController;

		String batchTableURL = performFishResponse.getMapParameters().get(PerformFishAnalyticsConstant.BATCHES_TABLE);

		if (batchTableURL == null || batchTableURL.isEmpty())
			Window.alert("Something seems wrong. No batches tables matching with parameter "
					+ PerformFishAnalyticsConstant.BATCHES_TABLE + " returned from service");

		final Map<String, List<String>> mapParameters = new HashMap<String, List<String>>();

		StringBuilder dataInputsFormatter = new StringBuilder();
		dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_INPUT_TABLE + "=" + batchTableURL + ";");

		// READ THE INPUT FILE
		String popType = viewController.getForm().getBatchType();

		DataMinerAlgorithms chartType = null;
		if (popType.equalsIgnoreCase(PerformFishAnalyticsConstant.BATCH_LEVEL.GROW_OUT_INDIVIDUAL_CLOSED_BATCHES.name())
				|| popType.equalsIgnoreCase(PerformFishAnalyticsConstant.BATCH_LEVEL.GROW_OUT_INDIVIDUAL.name())) {
			chartType = DataMinerAlgorithms.PERFORMFISH_SYNOPTICTABLE_BATCH;
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_QUARTER_LABELS + "="
					+ PerformFishAnalyticsConstant.DM_VALUE_ALL + ";");
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_ZONE_LABELS + "="
					+ PerformFishAnalyticsConstant.DM_VALUE_ALL + ";");
		} else if (popType
				.equalsIgnoreCase(PerformFishAnalyticsConstant.BATCH_LEVEL.HATCHERY_INDIVIDUAL_CLOSED_BATCHES.name())
				|| popType.equalsIgnoreCase(PerformFishAnalyticsConstant.BATCH_LEVEL.HATCHERY_INDIVIDUAL.name())) {
			chartType = DataMinerAlgorithms.PERFORMFISH_SYNOPTICTABLE_BATCH_HATCHERY;
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_STOCKING_PERIOD + "="
					+ PerformFishAnalyticsConstant.DM_VALUE_ALL + ";");
		} else if (popType.equalsIgnoreCase(PerformFishAnalyticsConstant.BATCH_LEVEL.PRE_ONGROWING.name()) || popType
				.equalsIgnoreCase(PerformFishAnalyticsConstant.BATCH_LEVEL.PRE_ONGROWING_CLOSED_BATCHES.name())) {
			chartType = DataMinerAlgorithms.PERFORMFISH_SYNOPTICTABLE_BATCH_PREGROW;
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_ZONE_LABELS + "="
					+ PerformFishAnalyticsConstant.DM_VALUE_ALL + ";");
			dataInputsFormatter.append(PerformFishAnalyticsConstant.DM_STOCKING_PERIOD + "="
					+ PerformFishAnalyticsConstant.DM_VALUE_ALL + ";");
		}

		String dataInParameters = dataInputsFormatter.toString();
		mapParameters.put(PerformFishAnalyticsConstant.DATA_INPUTS, Arrays.asList(dataInParameters));
		mapParameters.put("Identifier", Arrays.asList(
				"org.gcube.dataanalysis.wps.statisticalmanager.synchserver.mappedclasses.transducerers." + chartType));

		GWT.log("Calling DM service with client input parameters: " + mapParameters);

		final SubmitRequestPanel submitRequestPanel = new SubmitRequestPanel("", 1);
		// submitRequestPanel.getElement().addClassName("ext-horizontal-panel");

		// TODO
		final DataMinerAlgorithms algorithm = chartType;
		final String tabTitle = "Synoptic Table";
		final String tabDescr = PerformFishResources.INSTANCE.synopticTable().getText();
		final Tab tab = viewController.createTab(tabTitle + " #" + (viewController.currentNumberOfTab() + 1),
				PerformFishResources.INSTANCE.synopticTable().getText(), submitRequestPanel);
		HorizontalPanel res = new HorizontalPanel();

		savePDFButton = new Button();
		savePDFButton.getElement().getStyle().setMargin(10, Unit.PX);
		savePDFButton.setIcon(IconType.PRINT);
		savePDFButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				saveFilePDF(algorithm.getId(), tabTitle, tabTitle, tabDescr, requestId, outputIteration, outputNumber);
				// dmResponse.getListOutput().size()
			}
		});
		savePDFButton.setVisible(false);

		res.add(savePDFButton);
		submitRequestPanel.addWidget(res);

		final HTMLPanel panelContainer = submitRequestPanel.getContainerPanel();

		final LoaderIcon loaderIcon = new LoaderIcon(
				"Submitting request to " + DataMinerAlgorithms.PERFORMFISH_SYNOPTICTABLE_BATCH.getTitle() + "...");
		loaderIcon.setVisible(true);
		panelContainer.setVisible(true);
		panelContainer.add(loaderIcon);
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			@Override
			public void execute() {
				loaderIcon.setFocus(true);
			}
		});

		final DataMinerAlgorithms toFinalChartType = chartType;
		PerformFishAnalyticsServiceAsync.Util.getInstance().callDMServiceToLoadSynopticTable(performFishResponse,
				mapParameters, new AsyncCallback<DataMinerResponse>() {

					@Override
					public void onSuccess(DataMinerResponse dmResponse) {
						loaderIcon.setVisible(false);
						checkTabSpinner(submitRequestPanel, tab);
						// field_unary_algorithm.setVisible(true);
						GWT.log("I'm displaying: " + dmResponse);
						displayOutputFilesAsStaticEntities(dmResponse, toFinalChartType, null, null, null,
								panelContainer, false, outputIteration);
					}

					@Override
					public void onFailure(Throwable caught) {
						loaderIcon.setVisible(false);
						checkTabSpinner(submitRequestPanel, tab);
						displayOutputFilesAsStaticEntities(null, toFinalChartType, null, null, null, panelContainer,
								true, outputIteration);

					}
				});

	}

	/**
	 * Remove the spinner if all DM responses are returned.
	 *
	 * @param requestPanel
	 *            the request panel
	 * @param tab
	 *            the tab
	 */
	private void checkTabSpinner(SubmitRequestPanel requestPanel, Tab tab) {
		requestPanel.incrementCompletedRequests();
		int completed = requestPanel.getCompletedRequests();
		int total = requestPanel.getTotalRequests();

		if (completed >= total) {
			viewController.noSpinner(tab);
		}
	}

	private String getOutputId(int oIteration, boolean isLegend) {
		String outputId;
		if (isLegend) {
			outputId = "OutputId_" + requestId + "_" + oIteration + "_" + 1;
		} else {
			outputNumber++;
			outputId = "OutputId_" + requestId + "_" + oIteration + "_" + outputNumber;
		}
		return outputId;
	}

	private void displayOutputFilesAsStaticEntities(final DataMinerResponse dmResponse,
			final DataMinerAlgorithms chartType, List<KPI> inputKPIs, List<KPI> outputKPIs, final String focusID,
			final Panel container, boolean displayError, final int oIteration) {

		String title = displayError ? "No results " : "";

		if (displayError) {
			Alert alert = new Alert(title);
			alert.setType(AlertType.ERROR);
			alert.setClose(false);
			alert.getElement().getStyle().setMargin(10, Unit.PX);
			container.add(alert);
			return;
		}

		savePDFButton.setVisible(true);

		final String toTitle = title;

		outputNumber = 1;

		for (OutputFile outputFile : dmResponse.getListOutput()) {

			final FileContentType fileContentType = outputFile.getDataType();

			switch (outputFile.getDataType()) {
			case IMAGE:
				PerformFishAnalyticsServiceAsync.Util.getInstance().getImageFile(outputFile,
						new AsyncCallback<String>() {

							@Override
							public void onFailure(Throwable caught) {
								// showAlert(caught.getMessage(),
								// AlertType.ERROR, true,
								// uib_vp_deanalanlysis_request_container);
								Window.alert(caught.getMessage());

							}

							@Override
							public void onSuccess(String base64Content) {

								String title = toTitle;

								String outputId = getOutputId(oIteration, false);
								ShowResult showResult = new ShowResult(outputId, title, fileContentType);
								showResult.showImage(base64Content);
								container.add(showResult);

							}
						});
				break;
			case CSV:
				PerformFishAnalyticsServiceAsync.Util.getInstance().getCSVFile(outputFile, true,
						new AsyncCallback<CSVFile>() {

							@Override
							public void onFailure(Throwable caught) {
								// showAlert(caught.getMessage(),
								// AlertType.ERROR, true,
								// uib_vp_deanalanlysis_request_container);
								Window.alert(caught.getMessage());
							}

							@Override
							public void onSuccess(CSVFile result) {
								GWT.log("Displaying: " + result);

								String cssTableStyle = "simpletable";

								String title = toTitle;
								switch (chartType) {
								case PERFORMFISH_SYNOPTICTABLE_BATCH:
								case PERFORMFISH_SYNOPTICTABLE_BATCH_HATCHERY:
								case PERFORMFISH_SYNOPTICTABLE_BATCH_PREGROW:

									String outputId;
									if (!result.getFileName().contains("legend")) {
										cssTableStyle = "synoptictable-farm";
										outputId = getOutputId(oIteration, false);
									} else {
										cssTableStyle = "simpletable-synoptic";
										outputId = getOutputId(oIteration, true);
									}

									ShowResult showResultSin = new ShowResult(outputId, title, fileContentType);
									showResultSin.showCSVFile(result, cssTableStyle);
									displayingOrderedSynopticTable(dmResponse, result.getFileName(), container,
											showResultSin);

									return;
								case PERFORMFISH_SYNOPTIC_TABLE_FARM:
									break;
								default:
									break;
								}
								// ALL OTHER CASES

								String outputId = getOutputId(oIteration, false);
								ShowResult showResult = new ShowResult(outputId, title, fileContentType);
								showResult.showCSVFile(result, cssTableStyle);
								container.add(showResult);
							}
						});
				break;

			default:
				break;
			}

		}

	}

	private void displayingOrderedSynopticTable(final DataMinerResponse dmResponse, String fileName,
			final Panel container, ShowResult showResult) {
		int hashcode = dmResponse.hashCode();
		GWT.log("The hascode is: " + hashcode);
		FlexTable flex = synopticOrderBy.get(hashcode);

		if (flex == null) {
			GWT.log("The flextable is null");
			flex = new FlexTable();
			synopticOrderBy.put(hashcode, flex);
			container.add(flex);
		}

		if (fileName.toLowerCase().contains("legend")) {
			flex.setWidget(0, 0, showResult);
		} else {
			flex.setWidget(flex.getRowCount() + 1, 0, showResult);
		}
	}

	private static native void saveFilePDF(String chartType, String filename, String tabTitle, String tabDescr,
			int requestId, int outputIteration, int outputNumber)/*-{
		var that = this;
		console.log('saveFilePDF()');
		console.log('requestId: ' + requestId);
		console.log('OutputIteration: ' + outputIteration);
		console.log('OutputNumber:' + outputNumber);

		var tTitle = tabTitle;
		var tDescr = tabDescr;

		var pdoc = new jsPDF("p", "mm", "a4");
		pdoc.setProperties({
			title : 'PerformFish ' + tTitle,
			subject : ' Results',
			author : 'PerformFish',
			keywords : 'PerformFish',
			creator : 'D4Science'
		});

		var lMargin = 15; //left margin in mm
		var rMargin = 15; //right margin in mm
		var tMargin = 15; //top margin in mm
		var bMargin = 15; //bottom margin in mm
		var pdfWidthInMM = 210; // width of A4 in mm
		var pdfHeightInMM = 297; // height of A4 in mm
		var pageCenter = pdfWidthInMM / 2;

		pdoc.setFontSize(24);
		var title = "PerformFish " + tTitle;
		var titleHeight = pdoc.getLineHeight(title) / pdoc.internal.scaleFactor
		var xPos = lMargin;
		var yPos = tMargin;
		pdoc.text(title, pageCenter, yPos, 'center');
		yPos += titleHeight;

		pdoc.setFontSize(10);
		var lineHeight = pdoc.getLineHeight(tDescr) / pdoc.internal.scaleFactor
		var splittedAnalysisDescription = pdoc.splitTextToSize(tDescr,
				(pdfWidthInMM - lMargin - rMargin));
		var lines = splittedAnalysisDescription.length // splitted text is a string array
		var analysisDescriptionHeight = lines * lineHeight

		pdoc.text(splittedAnalysisDescription, xPos, yPos, 'left');
		yPos += analysisDescriptionHeight;

		//yPos += 2;
		//pdoc.text("Analysis:", xPos, yPos, 'left');
		yPos += 6;

		for (var i = 1; i <= outputIteration; i++) {

			for (var j = 1; j <= outputNumber; j++) {

				var outputId = 'OutputId_' + requestId + '_' + i + '_' + j;
				console.log('OutputId: ' + outputId);

				var resultOutputNumber = $doc.getElementById(outputId);

				console.log('OutputId_: ' + resultOutputNumber.innerHTML);
				var resultType = resultOutputNumber.className;

				console.log('resultOutputNumber className: ' + resultType);

				if (typeof resultType !== 'undefined' && resultType !== null
						&& resultType !== '') {

					if (resultType == 'csv') {
						console.log('Result Type csv: ' + i);
						// foo could get resolved and it's defined

						var childrenTable = resultOutputNumber.children;
						var titleCurrentTable = childrenTable[0].rows[0].cells[0];
						console.log('Title current table: '
								+ titleCurrentTable.textContent);
						yPos += 2;
						pdoc.setFontSize(10);
						pdoc.setFontType("bold");
						pdoc.text(titleCurrentTable.textContent, xPos, yPos,
								'left');
						yPos += 6;

						pdoc.setFontSize(10);
						pdoc.setFontType("normal");

						var secondDiv = childrenTable[0].rows[1].cells[0];
						var secondTable = secondDiv.children[0].children[0];

						console.log('CSV SecondTable: ' + secondTable);

						var checkColor = function(data) {
							if (data.section === 'body') {
								console.log(data);

								if (typeof data.cell !== 'undefined'
										&& data.cell !== null) {
									if (typeof data.cell.raw !== 'undefined'
											&& data.cell.raw !== null) {
										console.log(data.cell.raw.innerHTML);

										var good = 'background-color: limegreen;';
										var normal = 'background-color: yellow;';
										var bad = 'background-color: red;';

										if (data.cell.raw.innerHTML
												.indexOf(good) >= 0) {
											console.log('good');
											data.cell.styles.fillColor = [ 50,
													205, 50 ];

											//var posIW = (data.cell.x
											//		+ (data.cell.width / 2) - 2);
											//var posIH = (data.cell.y
											//		+ (data.cell.height / 2) - 2);
											//var greenImg = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wgARCAAKAAoDAREAAhEBAxEB/8QAFQABAQAAAAAAAAAAAAAAAAAAAAb/xAAWAQEBAQAAAAAAAAAAAAAAAAAACAn/2gAMAwEAAhADEAAAAbSM8+wB/8QAFBABAAAAAAAAAAAAAAAAAAAAIP/aAAgBAQABBQIf/8QAFBEBAAAAAAAAAAAAAAAAAAAAIP/aAAgBAwEBPwEf/8QAFBEBAAAAAAAAAAAAAAAAAAAAIP/aAAgBAgEBPwEf/8QAFBABAAAAAAAAAAAAAAAAAAAAIP/aAAgBAQAGPwIf/8QAFBABAAAAAAAAAAAAAAAAAAAAIP/aAAgBAQABPyEf/9oADAMBAAIAAwAAABAAD//EABQRAQAAAAAAAAAAAAAAAAAAACD/2gAIAQMBAT8QH//EABQRAQAAAAAAAAAAAAAAAAAAACD/2gAIAQIBAT8QH//EABQQAQAAAAAAAAAAAAAAAAAAACD/2gAIAQEAAT8QH//Z'
											//pdoc.addImage(greenImg, 'JPEG',
											//		posIW, posIH, 4, 4);
										} else {
											if (data.cell.raw.innerHTML
													.indexOf(normal) >= 0) {
												console.log('normal');
												data.cell.styles.fillColor = [
														255, 255, 0 ];
												//var posIW = (data.cell.x
												//		+ (data.cell.width / 2) - 2);
												//var posIH = (data.cell.y
												//		+ (data.cell.height / 2) - 2);
												//var yellowImg = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wgARCAAKAAoDAREAAhEBAxEB/8QAFQABAQAAAAAAAAAAAAAAAAAAAAn/xAAWAQEBAQAAAAAAAAAAAAAAAAAABwr/2gAMAwEAAhADEAAAAbEZ/wCqAD//xAAUEAEAAAAAAAAAAAAAAAAAAAAg/9oACAEBAAEFAh//xAAUEQEAAAAAAAAAAAAAAAAAAAAg/9oACAEDAQE/AR//xAAUEQEAAAAAAAAAAAAAAAAAAAAg/9oACAECAQE/AR//xAAUEAEAAAAAAAAAAAAAAAAAAAAg/9oACAEBAAY/Ah//xAAUEAEAAAAAAAAAAAAAAAAAAAAg/9oACAEBAAE/IR//2gAMAwEAAgADAAAAEAAP/8QAFBEBAAAAAAAAAAAAAAAAAAAAIP/aAAgBAwEBPxAf/8QAFBEBAAAAAAAAAAAAAAAAAAAAIP/aAAgBAgEBPxAf/8QAFBABAAAAAAAAAAAAAAAAAAAAIP/aAAgBAQABPxAf/9k='
												//pdoc.addImage(yellowImg,
												//		'JPEG', posIW, posIH,
												//		4, 4);
											} else {
												if (data.cell.raw.innerHTML
														.indexOf(bad) >= 0) {
													console.log('bad');
													data.cell.styles.fillColor = [
															255, 0, 0 ];
													//var posIW = (data.cell.x
													//		+ (data.cell.width / 2) - 2);
													//var posIH = (data.cell.y
													//		+ (data.cell.height / 2) - 2);
													//var redImg = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wgARCAAKAAoDAREAAhEBAxEB/8QAFQABAQAAAAAAAAAAAAAAAAAAAAj/xAAWAQEBAQAAAAAAAAAAAAAAAAAACAn/2gAMAwEAAhADEAAAAYvlPfwAf//EABQQAQAAAAAAAAAAAAAAAAAAACD/2gAIAQEAAQUCH//EABQRAQAAAAAAAAAAAAAAAAAAACD/2gAIAQMBAT8BH//EABQRAQAAAAAAAAAAAAAAAAAAACD/2gAIAQIBAT8BH//EABQQAQAAAAAAAAAAAAAAAAAAACD/2gAIAQEABj8CH//EABQQAQAAAAAAAAAAAAAAAAAAACD/2gAIAQEAAT8hH//aAAwDAQACAAMAAAAQAA//xAAUEQEAAAAAAAAAAAAAAAAAAAAg/9oACAEDAQE/EB//xAAUEQEAAAAAAAAAAAAAAAAAAAAg/9oACAECAQE/EB//xAAUEAEAAAAAAAAAAAAAAAAAAAAg/9oACAEBAAE/EB//2Q=='
													//pdoc.addImage(redImg,
													//		'JPEG', posIW,
													//		posIH, 4, 4);
												} else {

												}

											}

										}
									}
								}

							}
						};

						pdoc.autoTable({
							theme : 'grid',
							startY : yPos,
							pageBreak : 'auto',
							bodyStyles : {
								fontSize : 6,
								minCellWidth : 11
							},
							//didDrawCell : checkColor,
							didParseCell : checkColor,
							html : secondTable
						});

						yPos = pdoc.previousAutoTable.finalY + 6;
					} else {
						if (resultType == 'image') {
							console.log('Result Type image: ' + i);
							var childrenTable = resultOutputNumber.children;

							var titleCurrentImage = childrenTable[0].rows[0].cells[0];
							console.log('Title current image: '
									+ titleCurrentImage.textContent);
							yPos += 2;
							pdoc.setFontSize(10);
							pdoc.setFontType("bold");
							pdoc.text(titleCurrentImage.textContent, xPos,
									yPos, 'left');
							yPos += 6;

							pdoc.setFontSize(10);
							pdoc.setFontType("normal");

							var secondDiv = childrenTable[0].rows[1].cells[0];
							var imageElement = secondDiv.getElementsByTagName(
									'img').item(0);
							console.log('Image element: ' + imageElement);
							pdoc.addImage(imageElement, lMargin, yPos);
							console.log('Image Height: ' + imageElement.height);
							yPos += (imageElement.height * 0.26458333);

						} else {
							console.log('Result Type unknow: ' + i);
						}

					}
				} else {
					console.log('Result Type is undefined:' + i);
				}

				if (j == outputNumber) {

				} else {
					if (pdfHeightInMM - yPos < pdfHeightInMM / 7) {
						pdoc.addPage();
						yPos = tMargin; // Restart position
					}
				}

			}

		}

		var numberOfPages = pdoc.internal.getNumberOfPages();
		console.log('NumberOfPages: ' + numberOfPages);
		for (var k = 1; k <= numberOfPages; k++) {
			pdoc.setPage(k);
			console.log('CurrentPage: ' + k);
			var footner = 'Page ' + k + "/" + numberOfPages;
			pdoc.text(footner, pageCenter, pdfHeightInMM - 7, 'center');

		}

		console.log('Done');

		pdoc.save(filename);

	}-*/;
}
