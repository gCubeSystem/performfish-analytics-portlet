/**
 *
 */
package org.gcube.portlets.user.performfishanalytics.shared;


/**
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jan 11, 2019
 */
public interface GenericDao {

	public String getId();
}
