package org.gcube.portlets.user.performfishanalytics.client.request;

import java.io.Serializable;

import org.gcube.portlets.user.performfishanalytics.client.DataMinerAlgorithms;

public class RequestInfo implements Serializable {

	private static final long serialVersionUID = -2786256071405466977L;
	private int outputIteration;
	private int outputNumber;
	private DataMinerAlgorithms dataminerAlgorithms;
	
	public RequestInfo(){
		super();
	}
	
	public RequestInfo(int outputIteration, int outputNumber, DataMinerAlgorithms dataminerAlgorithms) {
		super();
		this.outputIteration = outputIteration;
		this.outputNumber = outputNumber;
		this.dataminerAlgorithms = dataminerAlgorithms;
	}

	public int getOutputIteration() {
		return outputIteration;
	}

	public void setOutputIteration(int outputIteration) {
		this.outputIteration = outputIteration;
	}

	public int getOutputNumber() {
		return outputNumber;
	}

	public void setOutputNumber(int outputNumber) {
		this.outputNumber = outputNumber;
	}

	public DataMinerAlgorithms getDataminerAlgorithms() {
		return dataminerAlgorithms;
	}

	public void setDataminerAlgorithms(DataMinerAlgorithms dataminerAlgorithms) {
		this.dataminerAlgorithms = dataminerAlgorithms;
	}

	@Override
	public String toString() {
		return "RequestInfo [outputIteration=" + outputIteration + ", outputNumber=" + outputNumber
				+ ", dataminerAlgorithms=" + dataminerAlgorithms + "]";
	}
	
	
	
	
}
