# Changelog



## [v1.0.0] [r4.23.0] - 2020-05-28

### Features

- Added PDF support [#17249]


## [v0.4.0] - 2019-11-07

### Features

- Enabled DEA Analysis [#17252]


## [v0.3.0] - 2019-07-07

### Features

- Add numbers to correlation charts [#17247]
- Change Speedometer to Performeter [#17225]
- Added synoptic tables for Pre-grow and Hatchery [#17570]



## [v0.2.0] - 2019-06-07

### Features

- Release Synoptic table functionality [#17166]



## [v0.1.0] - 2019-05-07

### Features

- First Release



This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).